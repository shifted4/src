package Week4Day1.Assignment2.View;

import Week4Day1.Assignment2.Controller.ControllerMahasiswa;

import java.util.Scanner;

public class ViewMahasiswa {
    public static void main (String args []){
        ControllerMahasiswa cm = new ControllerMahasiswa();
        Scanner input = new Scanner(System.in);

        int pil =0;

        do {
            System.out.println("Menu");
            System.out.println("1. Register Mahasiswa");
            System.out.println("2. Notification");
            System.out.print("Masukan pilihan : ");
            pil = input.nextInt();
            if (pil == 1) {
                cm.registerMahasiswa();
            } else if (pil == 2) {
                cm.notificationMahasiswa();
            }
        }while(pil != 3);
    }
}
