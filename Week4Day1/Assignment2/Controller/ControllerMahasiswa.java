package Week4Day1.Assignment2.Controller;

import Week4Day1.Assignment2.Model.ModelMahasiswa;

import java.util.Scanner;

public class ControllerMahasiswa {

    Scanner input = new Scanner(System.in);
    ModelMahasiswa mh = new ModelMahasiswa();

    public void registerMahasiswa(){
        System.out.println("// Register //");
        System.out.print("Input Id : ");
        int id = input.nextInt();

        System.out.print("Input Nama : ");
        String nama = input.next();
        System.out.print("Input Nilai Biologi : ");
        int nilai1 = input.nextInt();
        System.out.print("Input Nilai Fisika : ");
        int nilai2 = input.nextInt();
        System.out.print("Input Nilai Kimia : ");
        int nilai3 = input.nextInt();


        mh.setId_mahasiswa(id);
        mh.setNama_mahasiswa(nama);
        mh.setNilai1(nilai1);
        mh.setNilai2(nilai2);
        mh.setNilai3(nilai3);

        double rata2 = (nilai1 + nilai2 +nilai3) /3;

        System.out.println("Nilai rata2 "+mh.getNama_mahasiswa()+" Adalah : "+rata2);

        do{
            if (rata2 > 7){
                System.out.println("Mahasiswa Selesai didaftarkan ");
            }else {
                System.out.println("Maaf nilai Rata2 "+rata2+" Tidak Cukup!");
            }
        }while (false);

    }
    public void notificationMahasiswa(){
        System.out.println("// Notification //");

        System.out.println("Email terkirim kepada mahasiswa dengan nama "+mh.getNama_mahasiswa());
    }
}
