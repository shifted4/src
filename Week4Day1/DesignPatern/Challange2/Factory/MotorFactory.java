package Week4Day1.DesignPatern.Challange2.Factory;


import Week4Day1.DesignPatern.Challange2.AbstrakFactory.AbstractFactory;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Mobil;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Motor;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Pesawat;
import Week4Day1.DesignPatern.Challange2.Product.MotorGede;
import Week4Day1.DesignPatern.Challange2.Product.MotorSepeda;
import Week4Day1.DesignPatern.Challange2.Product.MotorSport;

public class MotorFactory extends AbstractFactory {

    @Override
    public Pesawat getPesawat(String pesawat) {
        return null;
    }

    @Override
    public Mobil getMobil(String mobil) {
        return null;
    }

    @Override
    public Motor getMotor(String motor) {
        if(motor == null){
            return null;
        }
        if(motor.equalsIgnoreCase("HARLEY")){
            return new MotorGede();
        } else if(motor.equalsIgnoreCase("BMX")){
            return new MotorSepeda();
        } else if(motor.equalsIgnoreCase("NINJA")){
            return new MotorSport();
        }
        return null;
    }
}
