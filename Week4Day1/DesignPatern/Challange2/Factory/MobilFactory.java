package Week4Day1.DesignPatern.Challange2.Factory;


import Week4Day1.DesignPatern.Challange2.AbstrakFactory.AbstractFactory;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Mobil;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Motor;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Pesawat;
import Week4Day1.DesignPatern.Challange2.Product.MobilJadul;
import Week4Day1.DesignPatern.Challange2.Product.MobilKeluarga;
import Week4Day1.DesignPatern.Challange2.Product.MobilSport;

public class MobilFactory extends AbstractFactory {

    @Override
    public Pesawat getPesawat(String pesawat) {
        return null;
    }

    @Override
    public Mobil getMobil(String mobil) {
               if (mobil == null){
                   return null;
               }
               if(mobil.equalsIgnoreCase("BMW")){
                   return new MobilJadul();
               } else if (mobil.equalsIgnoreCase("LAMBHORGINI")) {
                   return new MobilSport();
               }else if (mobil.equalsIgnoreCase("AVANZA")){
                   return new MobilKeluarga();
               }
        return null;
    }

    @Override
    public Motor getMotor(String motor) {
        return null;
    }
}//End of the BankFactory class.
