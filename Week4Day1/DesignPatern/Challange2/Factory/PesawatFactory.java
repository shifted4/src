package Week4Day1.DesignPatern.Challange2.Factory;

import Week4Day1.DesignPatern.Challange2.AbstrakFactory.AbstractFactory;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.*;
import Week4Day1.DesignPatern.Challange2.Product.PesawatHelikopter;
import Week4Day1.DesignPatern.Challange2.Product.PesawatHelikopterMiliter;
import Week4Day1.DesignPatern.Challange2.Product.PesawatTerbang;

public class PesawatFactory extends AbstractFactory {

    @Override
    public Pesawat getPesawat(String pesawat) {
        if(pesawat == null){
            return null;
        }
        if(pesawat.equalsIgnoreCase("GARUDA")){
            return new PesawatTerbang();
        } else if(pesawat.equalsIgnoreCase("HELI")){
            return new PesawatHelikopter();
        } else if(pesawat.equalsIgnoreCase("PREDATOR")){
            return new PesawatHelikopterMiliter();
        }
        return null;
    }

    @Override
    public Mobil getMobil(String mobil) {
        return null;
    }

    @Override
    public Motor getMotor(String motor) {
        return null;
    }
}//End of the BankFactory class.
