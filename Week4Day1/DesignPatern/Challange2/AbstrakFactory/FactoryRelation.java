package Week4Day1.DesignPatern.Challange2.AbstrakFactory;

import Week4Day1.DesignPatern.Challange2.Factory.PesawatFactory;
import Week4Day1.DesignPatern.Challange2.Factory.MobilFactory;
import Week4Day1.DesignPatern.Challange2.Factory.MotorFactory;

public class FactoryRelation {
             public static AbstractFactory getFactoryRelation(String choice){
              if(choice.equalsIgnoreCase("Pesawat")){
                 return new PesawatFactory();
              } else if(choice.equalsIgnoreCase("Mobil")){
                 return new MobilFactory();
              }else if(choice.equalsIgnoreCase("Motor")){
                  return new MotorFactory();
              }
              return null;
           }

}//End of the FactoryCreator.
