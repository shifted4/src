package Week4Day1.DesignPatern.Challange2.AbstrakFactory;

import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Pesawat;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Mobil;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Motor;
public abstract class AbstractFactory {
          public abstract Pesawat getPesawat(String pesawat);
          public abstract Mobil getMobil(String mobil);
          public abstract Motor getMotor(String motor);
        }
