package Week4Day1.DesignPatern.Challange2.Product;

import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Pesawat;

public class PesawatHelikopter implements Pesawat {
    private final String NamaPesawatHelikopter;
    public PesawatHelikopter(){
        NamaPesawatHelikopter="G1 HELI";
    }
    public String getNamaPesawat(){
        return NamaPesawatHelikopter;
    }
}