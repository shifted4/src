package Week4Day1.DesignPatern.Challange2.Product;


import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Motor;

public class MotorSport implements Motor {
    private final String NamaMotorSport;
    public MotorSport(){
        NamaMotorSport="NINJA R3";
    }

    @Override
    public String getNamaMotor() {
        return NamaMotorSport;
    }
}//End of the HomeLoan class.

