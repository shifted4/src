package Week4Day1.DesignPatern.Challange2.Product;

import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Mobil;

public class MobilKeluarga implements Mobil {
    private final String NamaMobilKeluarga;
    public MobilKeluarga(){
        NamaMobilKeluarga="AVANZA";
    }

    @Override
    public String getNamaMobil() {
        return NamaMobilKeluarga;
    }
}

