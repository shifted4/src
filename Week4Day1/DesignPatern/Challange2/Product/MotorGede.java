package Week4Day1.DesignPatern.Challange2.Product;

import Week4Day1.DesignPatern.Bank.Loan;
import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Motor;

public class MotorGede implements Motor {
    private final String NamaMotorGede;
    public MotorGede(){
        NamaMotorGede="HARLEY DAVISION";
    }

    @Override
    public String getNamaMotor() {
        return NamaMotorGede;
    }
}//End of the EducationLoan class.
