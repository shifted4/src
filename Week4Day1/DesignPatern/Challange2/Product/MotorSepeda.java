package Week4Day1.DesignPatern.Challange2.Product;


import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Motor;

public class MotorSepeda implements Motor {
    private final String NamaMotorSepeda;
    public MotorSepeda(){
        NamaMotorSepeda="BMX";
    }

    @Override
    public String getNamaMotor() {
        return NamaMotorSepeda;
    }
}