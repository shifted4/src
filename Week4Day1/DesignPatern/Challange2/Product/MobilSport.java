package Week4Day1.DesignPatern.Challange2.Product;

import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Mobil;

public class MobilSport implements Mobil {
    private final String NamaMobilSport;
    public MobilSport(){
        NamaMobilSport="LAMBHORGINI";
    }

    @Override
    public String getNamaMobil() {
        return NamaMobilSport;
    }
}