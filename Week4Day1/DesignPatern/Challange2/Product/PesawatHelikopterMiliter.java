package Week4Day1.DesignPatern.Challange2.Product;


import Week4Day1.DesignPatern.Challange2.InterfaceFactory.Pesawat;

public class PesawatHelikopterMiliter implements Pesawat {

    private final String NamaPesawatHelikopterMiliter;

    public PesawatHelikopterMiliter() {
        NamaPesawatHelikopterMiliter = "PREDATOR 1";
    }

    public String getNamaPesawat() {
        return NamaPesawatHelikopterMiliter;
    }
}