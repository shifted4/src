package Week4Day1.DesignPatern.Challange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FactoryApp {
    public static void main(String args[])throws IOException {

        GetProductFactory productFactory = new GetProductFactory();

        System.out.print("Masukan Product : ");
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

        String productName=br.readLine();
        System.out.print("Masukan jumlah product : ");
        int units=Integer.parseInt(br.readLine());

        Factory p = productFactory.getProduct(productName);
        //call getRate() method and calculateBill()method of DomesticPaln.

        System.out.print("Bill amount for "+productName+" of  "+units+" units is: ");
        p.getFound();
        p.calculateBill(units);
    }
}
