package Week4Day1.DesignPatern.Challange;

abstract class Factory {
    protected double found;
    abstract void getFound();
    public void calculateBill(int units){
        System.out.println(units*found);
    }
}
