package Week4Day1.DesignPatern.Challange;


class GetProductFactory {
    public Factory getProduct(String productType) {
        if (productType == null) {
            return null;
        }
        if (productType.equalsIgnoreCase("MOTORGEDE")) {
            return new MotorGedeProduct();
        } else if (productType.equalsIgnoreCase("SEPEDA")) {
            return new SepedaProduct();
        } else if (productType.equalsIgnoreCase("MOTORSPORT")) {
            return new MotorSportProduct();
        }
        return null;
    }
}
