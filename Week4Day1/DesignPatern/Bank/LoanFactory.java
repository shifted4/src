package Week4Day1.DesignPatern.Bank;

import Week4Day1.DesignPatern.Bank.Bank;
import Week4Day1.DesignPatern.Bank.BussinessLoan;
import Week4Day1.DesignPatern.Bank.EducationLoan;
import Week4Day1.DesignPatern.Bank.HomeLoan;
import Week4Day1.DesignPatern.Bank.Loan;

class LoanFactory extends AbstractFactory{
                   public Bank getBank(String bank){
                        return null;
                  }

             public Loan getLoan(String loan){
              if(loan == null){
                 return null;
              }
              if(loan.equalsIgnoreCase("Home")){
                 return new HomeLoan();
              } else if(loan.equalsIgnoreCase("Business")){
                 return new BussinessLoan();
              } else if(loan.equalsIgnoreCase("Education")){
                 return new EducationLoan();
              }
              return null;
           }

        }
