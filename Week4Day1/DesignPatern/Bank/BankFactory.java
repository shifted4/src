package Week4Day1.DesignPatern.Bank;

import Week4Day1.DesignPatern.Bank.Bank;
import Week4Day1.DesignPatern.Bank.BankHDFC;
import Week4Day1.DesignPatern.Bank.ICICI;
import Week4Day1.DesignPatern.Bank.SBI;
import Week4Day1.DesignPatern.Bank.Loan;

class BankFactory extends AbstractFactory{
           public Bank getBank(String bank){
              if(bank == null){
                 return null;
              }
              if(bank.equalsIgnoreCase("HDFC")){
                 return new BankHDFC();
              } else if(bank.equalsIgnoreCase("ICICI")){
                 return new ICICI();
              } else if(bank.equalsIgnoreCase("SBI")){
                 return new SBI();
              }
              return null;
           }
          public Loan getLoan(String loan) {
              return null;
           }
        }//End of the BankFactory class.
