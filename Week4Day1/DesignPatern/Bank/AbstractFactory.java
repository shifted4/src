package Week4Day1.DesignPatern.Bank;

import Week4Day1.DesignPatern.Bank.Bank;
import Week4Day1.DesignPatern.Bank.Loan;

abstract class AbstractFactory{
          public abstract Bank getBank(String bank);
          public abstract Loan getLoan(String loan);
        }
