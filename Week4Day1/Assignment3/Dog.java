package Week4Day1.Assignment3;

public class Dog implements Entity{

    public double getLongStay;
    String name = "Rufus";
    int age = 5;

    static int longStay = 4;

    static Dog Rufus = new Dog();


    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
    
    public  double getLongStay() {
        return longStay;
    }


    @Override
    public double Calculation() {
        return Rufus.getLongStay();
    }


    public static void main (String args[]){
        Calculation c = new Calculation();
        System.out.println(c.billStayDog(Rufus));
    }
}
