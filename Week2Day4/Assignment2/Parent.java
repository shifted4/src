package Week2Day4.Assignment2;

class Parent {
    public void home() {
        System.out.println("Parent's home");
    }
    public void car(){
        System.out.println("Parent's car");
    }
}
class Chilld extends Parent {

    public void home() {
        super.home();
    }
    public void car(){
        super.car();
        System.out.println("Chilld car");
    }
    public static void main(String args[]) {
        Chilld ch = new Chilld();
        ch.home();
        ch.car();
    }
}

