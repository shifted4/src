package Week2Day4.Assignment4;
import java.util.*;
public class Assignment4 {

        Map m = new HashMap();
        Scanner input = new Scanner(System.in);

        void inputDummy(){
            m.put(3,new Staff(3,"naufal", 9, "CEO"));
            m.put(1,new Staff(1,"aldi", 2, "Supervisor"));
            m.put(2,new Staff(2,"radit", 5,"Director"));

        }

        public void addStaff(){

            System.out.print("Input ID : ");
            int id = this.input.nextInt();
            System.out.print("Input Nama : ");
            String nama = this.input.next();
            System.out.print("Input Jabatan : ");
            String jabatan = this.input.next();
            int absensi = 1;

            Staff st = new Staff(id, nama, absensi, jabatan);

            m.put(id,st);

        }


        public void viewStaff(){

            Set set=m.entrySet();//Converting to Set so that we can traverse
            Iterator itr=set.iterator();
            System.out.println("ID \t Nama \t Jabatan \t Absensi/Hari");

            while(itr.hasNext()){

                //Converting to Map.Entry so that we can get key and value separately
                Map.Entry entry=(Map.Entry)itr.next();
                //Convert to Staff
                Staff st = (Staff) entry.getValue();
                System.out.println(entry.getKey()+"\t"+st.getNama()+"\t"+st.getJabatan()+"\t"+st.getAbsensi());
            }

        }

        public void tambahAbsensi() {

            System.out.print("Masukin ID: ");
            int id = this.input.nextInt();

            Set set = m.entrySet();//Converting to Set so that we can traverse
            Iterator itr = set.iterator();
            while (itr.hasNext()) {

                //Converting to Map.Entry so that we can get key and value separately
                Map.Entry entry = (Map.Entry) itr.next();
                //Convert to Staff
                Staff st = (Staff) entry.getValue();

                if (st.getId() == id) {
                    st.tambahAbsensi();
                    System.out.println("Berhasil menambahkan absensi");
                }
            }
        }

        public static void main(String args[]) {

            Assignment4 obj = new Assignment4();

            int pil=0;

            do {
                System.out.println("MENU");
                System.out.println("0. Buat Dummy Data");
                System.out.println("1. Buat Staff");
                System.out.println("2. Tambahkan Absensi");
                System.out.println("3. Tampilkan Data Staff");

                System.out.println("99. EXIT");

                System.out.print("Input nomor : ");
                pil = obj.input.nextInt();

                switch (pil) {
                    // performs addition between numbers
                    case 0:
                        obj.inputDummy();
                        break;
                    case 1:
                        obj.addStaff();
                        break;
                    case 2:
                        obj.tambahAbsensi();
                        break;
                    case 3:
                        obj.viewStaff();
                        break;

                }
                System.out.println();

            } while(pil != 99);

            // closing the scanner object
            obj.input.close();

        }


}

