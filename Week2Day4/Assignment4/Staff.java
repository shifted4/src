package Week2Day4.Assignment4;

abstract  class Worker{
    int id;
    String nama;
    int absensi;

    abstract  void tambahAbsensi();

}

public class Staff extends Worker{
    String jabatan;

    Staff(int id, String nama, int absensi,String jabatan){
        this.id = id;
        this.nama = nama;
        this.jabatan = jabatan;
        this.absensi = absensi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
    @Override
    void tambahAbsensi() {
        this.absensi++;
    }
}

