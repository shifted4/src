package Week2Day4.Assignment5.View;

import Week2Day4.Assignment5.Controller.StaffController;

import java.util.Scanner;

public class StaffApp {
    public static void main(String args[]) {

        //instansiasi objek controller
        StaffController sc = new StaffController();

        //instansiasi objek input dengan scanner
        Scanner input = new Scanner(System.in);
        int pil=0;

        //looping menu
        do {
            System.out.println("------------------------>");
            System.out.println("=======> MENU <=======");
            System.out.println("0. Buat Dummy Data");
            System.out.println("1. Buat Staff");
            System.out.println("2. Absensi Staff");
            System.out.println("3. Hitung Tunjangan");
            System.out.println("4. Hitung Total Gaji");
            System.out.println("5. Laporan Gaji");
            System.out.println("------------------------>");
            System.out.println("6. EXIT");

            System.out.print("Input Pilihan : ");
            pil = input.nextInt();

            switch (pil) {
                case 0:
                    //memanggil method menambahkan banyak data
                    sc.createDummy();
                    break;
                case 1 :
                    sc.addStaff();
                    break;
                case 2:
                    //memanggil method absensi
                    sc.absenStaff();
                    break;
                case 3:
                    //memanggil method menghitung tunjangan
                    sc.hitungTunjangan();
                    break;
                case 4 :
                    //memanggil method menghitung gaji
                    sc.hitungTotalGaji();
                    break;
                case 5:
                    //memanggil method sorting
                    sc.viewStaffSort();
                    break;
            }
            System.out.println();

        } while(pil != 6);

        // closing the scanner object
        input.close();

    }
}
