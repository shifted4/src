package Week2Day4.Assignment5.Controller;

import Week2Day4.Assignment5.Model.Staff;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

public class StaffController {

    //instansiasi Map
    Map m = new HashMap();

    //instansiasi input scanner
    Scanner input = new Scanner(System.in);

    //membuat variabel global
    int resultTmakan, totalGaji;

    //Case 0
    //method data dummy
    public void createDummy(){
        System.out.println("Creating Dummy Data");
        m.put(3,new Staff(3,"naufal", 2000000));
        m.put(1,new Staff(1,"aldi", 3000000));
        m.put(2,new Staff(2,"radit", 4000000));
        m.put(4,new Staff(2,"faris", 3000000));
    }

    //Case 1
    //method menambahkan staff
    public void addStaff(){

        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.input.next();
        System.out.print("Input Gaji Pokok : ");
        int gapok = this.input.nextInt();

        //instansiasi objek model kelas staff
        Staff st = new Staff(id, nama, gapok);
        //menambahkan objek ke Map
        this.m.put(id,st);
    }

    //Case 2
    //Method Absensi staff
    public void absenStaff(){

        System.out.print("Masukin ID: ");
        int id = this.input.nextInt();

        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if (st.getId() == id) {
                st.AbsensiMethod();
                System.out.println("Berhasil menambahkan absensi");
            }
        }
    }

    //Case 3
    //Method menghitung tunjangan
    public void hitungTunjangan(){

        System.out.print("Menghitung Seluruh Tunjangan Staff : ");

        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if (st.getId() == st.getId()) {
                resultTmakan = st.getAbsensi() * 20000;
                System.out.println("Berhasil menambahkan tunjangan ");
            }
        }
    }

    //Case 4
    //Method total gaji
    public void hitungTotalGaji(){
        System.out.print("Menghitung Seluruh Total Gaji Staff : ");
        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if (st.getId() == st.getId()) {
                totalGaji = st.getGapok() + resultTmakan;
                System.out.println("Berhasil menambahkan Total gaji ");
            }
        }
    }

    //Case 5
    //method menampilkan sorting
    public void viewStaffSort(){
        Set set=m.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        System.out.println("ID \t Nama \t Absensi \t Total Gaji");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();
            System.out.println(entry.getKey()+"\t"+st.getNama()+"\t"+st.getAbsensi()+"\t"+totalGaji);
        }
    }

}
