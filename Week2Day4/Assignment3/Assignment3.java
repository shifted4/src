package Week2Day4.Assignment3;

class Parent {

    int money;
    String nama;

    Parent (int cmoney, String cnama){
        this.money = cmoney;
        this.nama = cnama;
    }
    public void home() {
        System.out.println("Parent's home");
    }
    public void car(){
        System.out.println("Parent's car");
    }
}
class Chilld extends Parent {

    int money2 = 200;
    String nama2 = "budi";

    Chilld(int cmoney, String cnama) {
        super(cmoney, cnama);
    }


    public void car(){
        super.car();
        System.out.println("Chilld car");
    }

    public void parentInfo(){
        System.out.println("Parents Nama : "+super.nama);
        System.out.println("Parents Money : "+super.money);
    }

    public void chilldInfo(){
        System.out.println("Nama : "+nama2);
        System.out.println("Money : "+money2);
    }
    public static void main(String args[]) {
        Chilld ch = new Chilld(10, "aldi");
        ch.home();
        ch.car();

        ch.parentInfo();
        ch.chilldInfo();
    }
}


