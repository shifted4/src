package Week2Day4.Assignment1;

import java.util.*;

public class Staff extends Worker{

    Map mapStaff = new HashMap<>();

//    Staff(int id, String nama, String jabatan){
//        this.idKaryawan = id;
//        this.namaKaryawan = nama;
//        this.jabatanKaryawan = jabatan;
//    }

    Staff st = new Staff();
    protected String jabatanKaryawan;

    public String getJabatanKaryawan() {
        return jabatanKaryawan;
    }

    public void setJabatanKaryawan(String jabatanKaryawan) {
        this.jabatanKaryawan = jabatanKaryawan;

    }

    public void addStaff(){

        Scanner inputInt = new Scanner(System.in);
        Scanner inputString = new Scanner(System.in);

        System.out.print("Masukan ID Karyawan : ");
        super.idKaryawan = inputInt.nextInt();
        st.setIdKaryawan(idKaryawan);

        System.out.print("Masukan Nama Karyawan : ");
        super.namaKaryawan = inputString.next();
        st.setNamaKaryawan(namaKaryawan);

        System.out.print("Masukan Jabatan Karyawan : ");
        jabatanKaryawan = inputString.next();
        st.setJabatanKaryawan(jabatanKaryawan);

        mapStaff.put(1, st);

        inputInt.close();
        inputString.close();
    }

    public void showStaff(){
        Set set=mapStaff.entrySet();
        Iterator itr=set.iterator();
        while(itr.hasNext()){
            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
        System.out.println("ID \t Nama \t Jabatan");
        System.out.println(st.getIdKaryawan()+"\t"+st.getNamaKaryawan()+"\t"+st.getJabatanKaryawan());
    }
}
