package Week2Day4.Assignment1;

import java.util.Scanner;

public class StaffApp {
    public static void main(String args[]) {

        Scanner inputInt = new Scanner(System.in);

        Staff st = new Staff();

        int pil = 0;

        //looping menu
        do {
            System.out.println("------------------------>");
            System.out.println("=======> MENU <=======");
            System.out.println("1. Buat Staff");
            System.out.println("2. Tampilkan Laporan Staff");
            System.out.println("------------------------>");
            System.out.println("3. EXIT");

            System.out.print("Input nomor : ");
            pil = inputInt.nextInt();

            switch (pil) {
                // pilihan kondisi menu
                case 1:
                    st.addStaff();
                    break;
                case 2:
                    st.showStaff();
                    break;
                default:
                    break;
            }
        } while (pil != 3);
        inputInt.close();
    }


}
