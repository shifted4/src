package Week3Day1.Assignment2.Socket;

import java.io.*;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;

public class MyClient {
    public static void main(String[] args) throws IOException {

        MyClientController cl = new MyClientController();
        Scanner input = new Scanner(System.in);

        cl.login();


        int pil =0;
        do {
            System.out.println("---------------------------");
            System.out.println("==========> MENU <=========");
            System.out.println("1. Connect Socket");
            System.out.println("2. Send Data to Server");
            System.out.println("3. Close Socket");
            System.out.println("4. EXIT");
            System.out.println("---------------------------");

            System.out.print("Input nomor : ");
            pil = input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    cl.clientUp();
                    break;

                case 2:
                    cl.readFile();
                    cl.sentToServer();
                    break;
                case 3:
                    cl.clientDown();
                    break;

                default:
                    System.out.println("Isi sesuai pilihan 1 - 4 !");
                    break;

            }
            System.out.println();

        } while(pil != 4);


    }

}