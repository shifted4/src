package Week3Day1.Assignment2.Socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MyServer {
    public void login() {

        Scanner input = new Scanner(System.in);
        boolean checkRegex;

        while (true) {

            System.out.println("======> Login <======");
            System.out.print("Username : ");
            String username = input.next();
            System.out.print("Password : ");
            String password = input.next();


            String regexU = "^[A-Za-z0-9+_.-]+@(.+)$";
            boolean regexUsername = Pattern.matches(regexU, username);
            String regexP = ".{8,32}" + "[a-zA-z]*" + "\\w*";
            boolean regexPassword = Pattern.matches(regexP, password);

            if (regexUsername == true && regexPassword == true){
                checkRegex = true;
            }else {
                checkRegex = false;
            }

            if (checkRegex == true) {
                System.out.println(">>>>>>>>>>>>>> Login Sukse <<<<<<<<<<<<<<");
                System.out.println("Selamat \"" + username + "\" Anda Berhasil LOGIN !");
                System.out.println(">>>>>>>>>>>>>>>>>Selamat!<<<<<<<<<<<<<<<<");
                break;
            } else {
                System.out.println("Maaf Username dan Password SALAH !");
            }

        }
    }

        //Read Properties File
        int port = 0;
        String ip = "";

        public void getPropValues() throws IOException {
            System.out.println("Read Config");
            InputStream inputStream = null;

            try {
                Properties prop = new Properties();
                String propFileName = "config.properties";

                inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
                if (inputStream != null) {
                    prop.load(inputStream);
                } else {
                    throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
                }

                // get the property value and print it out
                this.port = Integer.parseInt(prop.getProperty("PORT"));
                this.ip = prop.getProperty("IP");

                System.out.println("PORT = "+port);
                System.out.println("IP = "+ip);

            } catch (Exception e) {
                System.out.println("Exception: " + e);
            } finally {
                inputStream.close();
            }

        }

        ServerSocket ss;
        Socket s;
        DataInputStream dis;
        DataOutputStream dout;
        Scanner input;
        String serverMsg;

        public void serverUp(){
            System.out.println("Binding Port");

            try{
                this.ss=new ServerSocket(this.port);
                this.s=ss.accept();//establishes connection

                dis=new DataInputStream(s.getInputStream());
                dout=new DataOutputStream(s.getOutputStream());

            }catch(Exception e){System.out.println(e);}
        }

        public void serverDown() throws IOException {
            this.ss.close();
        }

        public void serverRead() throws IOException {
            System.out.println("Read Data");

            input  = new Scanner(System.in);
            String msg = "";

            String  str=(String)dis.readUTF();

            System.out.println("Data from Client = ");

            String [] parsedStrGet = str.trim().split("\n");
            for (int i = 0; i < parsedStrGet.length; i++) {
                String [] parsedStrGet2 = parsedStrGet[i].trim().split(",");

                System.out.println("Nama :"+parsedStrGet2[0]);
                System.out.println("Nilai 1 :"+parsedStrGet2[1]);
                System.out.println("Nilai 2 :"+parsedStrGet2[2]);
                System.out.println("Nilai 3 :"+parsedStrGet2[3]);

            }

            dout.close();

        }


        public static void main(String[] args) throws IOException {

            MyServer obj = new MyServer();

            obj.login();

            //Baca Prop
            obj.getPropValues();

            //Server Make Connection
            obj.serverUp();

            obj.serverRead();

            obj.serverDown();
        }

}