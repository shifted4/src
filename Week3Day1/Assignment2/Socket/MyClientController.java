package Week3Day1.Assignment2.Socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MyClientController {

    String data = "";
    public void login() {

        Scanner input = new Scanner(System.in);
        boolean checkRegex;

        while (true) {

            System.out.println("======> Login <======");
            System.out.print("Username : ");
            String username = input.next();
            System.out.print("Password : ");
            String password = input.next();


            String regexU = "^[A-Za-z0-9+_.-]+@(.+)$";
            boolean regexUsername = Pattern.matches(regexU, username);
            String regexP = ".{8,32}" + "[a-zA-z]*" + "\\w*";
            boolean regexPassword = Pattern.matches(regexP, password);

            if (regexUsername == true && regexPassword == true){
                checkRegex = true;
            }else {
                checkRegex = false;
            }

            if (checkRegex == true) {
                System.out.println(">>>>>>>>>>>>>> Login Sukse <<<<<<<<<<<<<<");
                System.out.println("Selamat \"" + username + "\" Anda Berhasil LOGIN !");
                System.out.println(">>>>>>>>>>>>>>>>>Selamat!<<<<<<<<<<<<<<<<");
                break;
            } else {
                System.out.println("Maaf Username dan Password SALAH !");
            }

        }
    }

    public void readFile(){
        try {

            FileReader fr=new FileReader("C:\\tmp\\data.txt");
            BufferedReader br=new BufferedReader(fr);

            int i;
            while((i=br.read())!=-1){
                this.data = this.data + (char) i;
            }

            //data di variabel lokal data

            br.close();
            fr.close();

        }catch (Exception e){
            System.out.println(e);
        }
    }

    Socket s;
    DataInputStream dis;
    DataOutputStream dout;
    String str;
    public void clientUp(){
        System.out.println("Connect ke Socket");
        try{
            this.s=new Socket("localhost",6666);

            this.dout=new DataOutputStream(s.getOutputStream());
            this.dis=new DataInputStream(s.getInputStream());

        }catch(Exception e){System.out.println(e);}
    }

    public void sentToServer() throws IOException {
        dout.writeUTF(data);
        dout.flush();
        System.out.println("==> koneksi berhasil!");
    }

    public void clientDown() throws IOException {
        s.close();
    }

}
