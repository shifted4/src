package Week3Day1.Assignment1.Socket;

import Week3Day1.Assignment1.Properties.CrunchifyGetPropertyValues;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class MyServer {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        ServerSocket ss = null; //instansiasi server socket
        DataInputStream din = null;
        DataOutputStream dout = null;

        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();

        try {
            // Load config.properties
            int port = Integer.parseInt(config.getPropValues("PORT"));

            ss = new ServerSocket(port);

            System.out.println("Server menunggu request dari Client");

            Socket socket = ss.accept(); //menunggu socket dari client
            din = new DataInputStream(socket.getInputStream());

            OutputStream outputStream = socket.getOutputStream();
            dout = new DataOutputStream(outputStream);

            String textDariClient = "";
            String textKeClient = "";

            while (!textDariClient.equalsIgnoreCase("exit")) {
                textDariClient = din.readUTF();
                System.out.println("Pesan dari Client: " + textDariClient);
                textKeClient = input.nextLine();
                dout.writeUTF(textKeClient);
                dout.flush();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                input.close();
                din.close();
                dout.close();
                ss.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}