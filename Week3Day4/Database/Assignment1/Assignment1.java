package Week3Day4.Database.Assignment1;

import Week2Day4.Assignment4.Assignment4;

import java.sql.*;
import java.util.Scanner;

public class Assignment1 {
    Statement stmt;
    ResultSet rs;
    Connection con;
    Scanner input = new Scanner(System.in);
    public void connectDatabase(){
        try{
            // 1. Register Driver Class
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2. Creating Connection

            con= DriverManager.getConnection("jdbc:mysql://localhost:3306/sonoo","root","230400");
            //here sonoo is database name, root is username and password

            // 3. Create Statement
            stmt=con.createStatement();

            // 4. Query Execution
            // 4.1 Insert, update & delete Query
            // INSERT INTO table_name (column1, column2, column3, ...)
            // VALUES (value1, value2, value3, ...);
            // stmt.executeUpdate("insert into emp (name,age) values ('Harry', 36)");
            // UPDATE table_name
            // SET column1 = value1, column2 = value2, ...
            // WHERE condition;
            // stmt.executeUpdate("update emp set age = 38 where id = 17");
//            stmt.executeUpdate("update emp set age = 20 where name like 'T%'");

            // 4.X Select Query

        }catch(Exception e){ System.out.println(e);}
    }


    public void showRecord() throws SQLException {
        rs = stmt.executeQuery("select * from emp");
        while(rs.next())
            System.out.println(rs.getInt(1)+
                    "  "+rs.getString(2)+
                    "  "+rs.getString(3));
    }
    public void inputRecord () throws SQLException {
        String nama = "";
        int umur = 0;
        System.out.print("Input Nama :");
        nama = input.next();
        System.out.print("Input Umur : ");
        umur = input.nextInt();
        stmt.executeUpdate("insert into emp (name,age) values ('"+ nama+ "', "+umur+")");
    }
    public void editRecord () throws SQLException {
        rs = stmt.executeQuery("select * from emp");
        String nama = "";
        int umur = 0;
        int id = 0;

        System.out.print("Input Id :");
        id = input.nextInt();

        if (rs.getInt(1) == id){
            System.out.println("Data ditemukan");
        }else {
            System.out.println("data tidak ada");
        }

        int pil = 0;
        System.out.println("Pilih 1 untuk edit Nama \nPilih 2 untuk edit age");
        switch (pil){
            case 1 :
                System.out.print("Data "+id+" Input Nama :");
                nama = input.next();
                stmt.executeUpdate("update emp set nama = "+nama+" where id = "+id);
                break;
            case 2 :
                System.out.print("Data "+id+" Input Umur : ");
                umur = input.nextInt();
                stmt.executeUpdate("update emp set nama = "+umur+" where id = "+id);
                break;
        }
    }
    public void deleteRecord () throws SQLException {
        rs = stmt.executeQuery("select * from emp");
        int id = 0;

        System.out.print("Input Id :");
        id = input.nextInt();
        if (rs.getInt(1) == id){
            System.out.println("Data ditemukan");
        }else {
            System.out.println("data tidak ada");
        }
        System.out.print("Masukan id yang akan di delete :");
        id = input.nextInt();
        rs = stmt.executeQuery("delete emp where id ="+id);

    }
    public void closeConnectionDatabase()throws Exception{
        //close connection
        con.close();
    }

    public static void main(String args[]) throws Exception {
        Assignment1 ass = new Assignment1();
        Scanner input = new Scanner(System.in);
        ass.connectDatabase();

        int pil = 0;
        do {
            System.out.println("---------------------------");
            System.out.println("==========> MENU <=========");
            System.out.println("1. Show All Record");
            System.out.println("2. Insert Record");
            System.out.println("3. Update Record");
            System.out.println("4. Delete Record");
            System.out.println("99. Exit");
            System.out.println("---------------------------");

            System.out.print("Input nomor : ");
            pil = input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    ass.showRecord();
                    break;
                case 2:
                    ass.inputRecord();
                    break;
                case 3:
                    ass.editRecord();
                    break;
                case 4:
                    ass.deleteRecord();
                    break;
                case 99:
                    ass.closeConnectionDatabase();
                    break;
            }
        } while (pil != 99);
    }

}
