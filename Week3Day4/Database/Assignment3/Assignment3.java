package Week3Day4.Database.Assignment3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Scanner;

public class Assignment3 {

    public static void main(String args[]) throws Exception {

        Scanner input = new Scanner(System.in);
        Connection con;
        Statement stmt;
        BufferedReader br;
        ResultSet rs;
        int id = 0;
        String name = "";
        int age = 0;
        String query = "";


            // 1. Register Driver Class
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2. Creating Connection

            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sonoo","root","230400");
            //here sonoo is database name, root is username and password

            con.setAutoCommit(false);

            stmt = con.createStatement();

            br = new BufferedReader(new InputStreamReader(System.in));


        int pil = 0;
        do {
            System.out.println("---------------------------");
            System.out.println("==========> MENU <=========");
            System.out.println("1. Show All Record");
            System.out.println("2. Insert Record");
            System.out.println("3. Update Record");
            System.out.println("4. Delete Record");
            System.out.println("99. Exit");
            System.out.println("---------------------------");

            System.out.print("Input nomor : ");
            pil = input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:

                    System.out.println("ID\tName\tAge");
                    query = "select * from emp";
                    rs = stmt.executeQuery(query);
                    while(rs.next())
                        System.out.println(rs.getInt(1)+
                                "  "+rs.getString(2)+
                                "  "+rs.getString(3));
                    break;
                case 2:

                    try{
                        while(true) {
                            query ="insert into emp values(?,?,?)";
                            PreparedStatement psInsert = (PreparedStatement) con.prepareStatement(query);

                                System.out.print("1. Enter id : ");
                                String s1 = br.readLine();
                                id = Integer.parseInt(s1);

                                System.out.print("2, Enter name : ");
                                name = br.readLine();

                                System.out.print("3. Enter age : ");
                                String s3 = br.readLine();
                                age = Integer.parseInt(s3);

                                psInsert.setInt(1, id);
                                psInsert.setString(2, name);
                                psInsert.setInt(3, age);

                                psInsert.executeUpdate();
                                System.out.print("Choice (\"commit\"/\"rollback\")");
                                String answer = br.readLine();

                                if (answer.equals("commit")) {
                                    con.commit();
                                    System.out.println("Record Add !!");
                                }
                                if (answer.equals("rollback")) {
                                    con.rollback();
                                    System.out.println("Record Cancel !!");
                                }

                                System.out.println("Want to add more records ? y/n");
                                String ans = br.readLine();

                                if (ans.equals("n")) {
                                    System.out.println("");
                                    break;
                                }
                            con.commit();
                            System.out.println("Record successfully saved");

                        }

                    }catch(Exception e){System.out.println(e);}
                    break;
                case 3:
                    System.out.println("// Edit Data //");
                    System.out.print("Masukan id : ");
                    String s1 = br.readLine();
                    id = Integer.parseInt(s1);

                    int pilihan = 0;
                    System.out.println("" +
                            "Pilih 1 untuk edit Nama \n" +
                            "Pilih 2 untuk edit age \n" +
                            "Pilih 3 untuk edit Name dan age \n"
                    );
                    System.out.print("Masukan pilihan : ");
                    pilihan = input.nextInt();
                    switch (pilihan) {
                        case 1:
                            while (true) {
                                System.out.print("Data " + id + " Input Nama :");
                                name = input.next();

                                query = "update emp set name = ? where id = ?";
                                PreparedStatement psUpdate = (PreparedStatement) con.prepareStatement(query);
                                psUpdate.setString(1, name);
                                psUpdate.setInt(2, id);

                                psUpdate.executeUpdate();
                                System.out.print("Choice (\"commit\"/\"rollback\")");
                                String answer = br.readLine();

                                if (answer.equals("commit")) {
                                    con.commit();
                                    System.out.println("Record Add !!");
                                }
                                if (answer.equals("rollback")) {
                                    con.rollback();
                                    System.out.println("Record Cancel !!");
                                }
                                con.commit();
                                System.out.println("Record successfully saved");
                                break;
                            }
                        case 2:
                            while (true) {
                                System.out.print("Data " + id + " Input Umur :");
                                age = input.nextInt();

                                query = "update emp set age = ? where id = ?";
                                PreparedStatement psUpdate = (PreparedStatement) con.prepareStatement(query);
                                psUpdate.setInt(1, age);
                                psUpdate.setInt(2, id);

                                psUpdate.executeUpdate();

                                System.out.print("Choice (\"commit\"/\"rollback\")");
                                String answer = br.readLine();

                                if (answer.equals("commit")) {
                                    con.commit();
                                    System.out.println("Record Add !!");
                                }
                                if (answer.equals("rollback")) {
                                    con.rollback();
                                    System.out.println("Record Cancel !!");
                                }
                                con.commit();
                                System.out.println("Record successfully saved");

                                break;
                            }
                        case 3:
                            while (true) {
                                System.out.print("Data " + id + " Input Nama :");
                                name = input.next();
                                System.out.print("Data " + id + " Input Umur :");
                                age = input.nextInt();

                                query = "update emp set name = ?, age = ? where id = ?";
                                PreparedStatement psUpdate = (PreparedStatement) con.prepareStatement(query);
                                psUpdate.setString(1, name);
                                psUpdate.setInt(2, age);
                                psUpdate.setInt(3, id);

                                psUpdate.executeUpdate();

                                System.out.print("Choice (\"commit\"/\"rollback\")");
                                String answer = br.readLine();

                                if (answer.equals("commit")) {
                                    con.commit();
                                    System.out.println("Record Add !!");
                                }
                                if (answer.equals("rollback")) {
                                    con.rollback();
                                    System.out.println("Record Cancel !!");
                                }
                                con.commit();
                                System.out.println("Record successfully saved");

                                break;
                            }
                    }
                    break;
                case 4:
                    while (true) {
                        System.out.println("// Delete Data //");
                        System.out.print("Masukan id yg akan dihapus : ");
                        s1 = br.readLine();
                        id = Integer.parseInt(s1);

                        query = "delete from emp where id = ?";
                        PreparedStatement psUpdate = (PreparedStatement) con.prepareStatement(query);
                        psUpdate.setInt(1, id);

                        psUpdate.executeUpdate();
                        System.out.print("Choice (\"commit\"/\"rollback\")");
                        String answer = br.readLine();

                        if (answer.equals("commit")) {
                            con.commit();
                            System.out.println("Record Add !!");
                        }
                        if (answer.equals("rollback")) {
                            con.rollback();
                            System.out.println("Record Cancel !!");
                        }
                        con.commit();
                        System.out.println("Record successfully saved");
                        break;
                    }
                    break;
                case 99:
                    con.close();
                    break;
            }
        } while (pil != 99);
    }

}
