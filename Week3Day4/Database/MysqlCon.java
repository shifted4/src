package Week3Day4.Database;

import java.sql.*;
class MysqlCon{
    public static void main(String args[]){
        try{
            // 1. Register Driver Class
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2. Creating Connection

            Connection con=DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo","root","P@ssw0rd");
            //here sonoo is database name, root is username and password

            // 3. Create Statement
            Statement stmt=con.createStatement();

            // 4. Query Execution
            // 4.1 Insert, update & delete Query
            // INSERT INTO table_name (column1, column2, column3, ...)
            // VALUES (value1, value2, value3, ...);
            // stmt.executeUpdate("insert into emp (name,age) values ('Harry', 36)");
            // UPDATE table_name
            // SET column1 = value1, column2 = value2, ...
            // WHERE condition;
            // stmt.executeUpdate("update emp set age = 38 where id = 17");
            stmt.executeUpdate("update emp set age = 20 where name like 'T%'");

            // 4.X Select Query
            ResultSet rs = stmt.executeQuery("select * from emp");
            while(rs.next())
                System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));

            // 5. Close Connection
            con.close();
        }catch(Exception e){ System.out.println(e);}
    }
}