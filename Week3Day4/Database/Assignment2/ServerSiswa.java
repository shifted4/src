package Week3Day4.Database.Assignment2;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;

public class ServerSiswa {
    public static void main(String[] args) {

        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();

        ResultSet rs =null;

        try {
            // 1. Register Driver Class
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2. Creating Connection
            Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/siswa","root","230400");
            //here sonoo is database name, root is username and password
            // 3. Create Statement
            Statement stmt=con.createStatement();


            // Load config.properties
            int port = Integer.parseInt(config.getPropValues("PORT"));
            ServerSocket ss=new ServerSocket(port);

            System.out.println("Menunggu Client");
            Socket s=ss.accept();//establishes connection

            DataInputStream dis=null;
            String getDataFromClient="";
            String mode = "";


            while(!mode.equalsIgnoreCase("exit")){
                dis=new DataInputStream(s.getInputStream());
                getDataFromClient=(String)dis.readUTF();

                JSONParser parser = new JSONParser();
                Reader reader = new StringReader(getDataFromClient);
                Object jsonObj = parser.parse(reader);
                JSONObject jsonObject = (JSONObject) jsonObj;

                mode = (String) jsonObject.get("mode");

                if(mode.equals("1")){
                    // Mode 1 - Registration
                    System.out.println("Mode "+mode);

                    String nama = (String) jsonObject.get("nama");
                    String username = (String) jsonObject.get("username");
                    String password = (String) jsonObject.get("password");

                    System.out.println(username);
                    System.out.println(password);

                    stmt.executeUpdate("insert into tbl_siswa (name,username,password) values ('"+nama+"','"+username+"','"+password+"');");
                    System.out.println("Berhasil menambahkan data siswa");

                }else if(mode.equalsIgnoreCase("2")) {
                    // Mode 2 - Login
                    System.out.println("Mode "+mode);
                    String username = (String) jsonObject.get("username");
                    String password = (String) jsonObject.get("password");

                    rs = stmt.executeQuery("select * from tbl_siswa where username = '"+username+"' AND password = '"+password+"'");

                    while(rs.next()){
                        if((username.equals(rs.getString("username")) && (password.equals(rs.getString("password"))))){
                            System.out.println("Client Berhasil Login");
                        }
                    }

                } else if (mode.equalsIgnoreCase("3")) {
                    // Mode 3 - Update
                    System.out.println("Mode "+mode);
                    System.out.println("Nilai berhasil ditambahkan !");
                    String pilih = (String) jsonObject.get("pilih");
                    String username = (String) jsonObject.get("username");
                    String password = (String) jsonObject.get("password");
                    ArrayList<Long> nilai = (ArrayList<Long>) jsonObject.get("nilai");



                    rs = stmt.executeQuery("select * from tbl_siswa where username = '"+username+"' AND password = '"+password+"'");

                    String sql = "insert into tbl_nilai (id_siswa,nilai) values (?,?)";
                    PreparedStatement psUpdate = con.prepareStatement(sql);

                    while(rs.next()){
                        for (long n: nilai) {
                            psUpdate.setInt(1, Integer.parseInt(rs.getString("id")));
                            psUpdate.setLong(2, n);
                            psUpdate.executeUpdate();
//                            con.commit();
                        }
                    }

                }else if(mode.equalsIgnoreCase("4")){
                    // Mode 4 - Tampil semua kecuali password
                    System.out.println("Mode "+mode);
                    System.out.println("Data berhasil ditampilkan!");
                    rs = stmt.executeQuery("select * from tbl_siswa");

                    String response = "";
                    while(rs.next())
                        response += rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3) +"\n";

                    DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                    dout.writeUTF(String.valueOf(response));
                    dout.flush();


                }else{
                    System.out.println("Sedih ah stuck");
                }
            }

        }catch (Exception e){
            System.out.println(e);
        }
    }

}
