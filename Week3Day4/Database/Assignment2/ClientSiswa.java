package Week3Day4.Database.Assignment2;

import Week3Day4.Database.Assignment2.Siswa;
import org.json.simple.JSONObject;

import javax.naming.PartialResultException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ClientSiswa {

    public static void main(String args[]) throws IOException {

        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
        Validasi val = new Validasi();
        Scanner input = new Scanner(System.in);

        String nama="", username="", password="";


        try {
            // Mengakses config.properties
            int port = Integer.parseInt(config.getPropValues("PORT"));
            String ip = config.getPropValues("IP");

            Socket s=new Socket(ip,port);
            DataOutputStream dout= null;
            DataInputStream din = null;


            int userInputMenu = 0;

            while (userInputMenu != 5) {
                System.out.println("Menu");
                System.out.println("1. Registration");
                System.out.println("2. Login");
                System.out.println("3. Update Data");
                System.out.println("4. Multithread Proses");
                System.out.println("5. Exit");
                System.out.print("Pilih Menu: ");
                userInputMenu = input.nextInt();

                switch (userInputMenu) {
                    case 1:
                        System.out.println("Anda Memilih Registration");
                        System.out.print("Input Nama: ");
                        nama = input.next();
                        System.out.print("Input Username: ");
                        username = input.next();
                        System.out.print("Input Password: ");
                        password = input.next();

                        boolean validEmail = val.validateEmail(username);
                        boolean validPassword = val.validatePassword(password);

//                        if(validEmail && validPassword) {
                        if(true){
                            JSONObject dataUserRegister = new JSONObject();

                            dataUserRegister.put("mode","1");
                            dataUserRegister.put("nama", nama);
                            dataUserRegister.put("username", username);
                            dataUserRegister.put("password", password);

                            dout = new DataOutputStream(s.getOutputStream());


                            // Harus cek lagi cara split ||
                            dout.writeUTF(String.valueOf(dataUserRegister));
                            dout.flush();
                        }else{
                            System.out.println("Email tidak valid");
                        }
                        break;

                    case 2:
                        System.out.println("Login");

                        System.out.print("Masukan username: ");
                        username = input.next();
                        System.out.print("Masukan password: ");
                        password = input.next();

                        boolean validEmailLogin = val.validateEmail(username);
                        boolean validPasswordLogin = val.validatePassword(password);

//                        if(validEmailLogin && validPasswordLogin) {
                        if(true){
                            JSONObject dataUserLogin = new JSONObject();
                            dataUserLogin.put("mode", "2");
                            dataUserLogin.put("username", username);
                            dataUserLogin.put("password", password);

                            dout = new DataOutputStream(s.getOutputStream());
                            dout.writeUTF(String.valueOf(dataUserLogin));
                            dout.flush();

//                            din = new DataInputStream(s.getInputStream());
//                            System.out.println(din.readUTF());
                        }else {
                            System.out.println("Email & Password tidak valid");
                        }
                        break;


                    case 3:
                        /*System.out.println("Update Data");
                        System.out.println("a. update data info ");
                        System.out.println("b. update data nilai");
                        System.out.print("pilih: ");
                        String pilih = input.next();

                        if(pilih.equals("a")){

                            System.out.print("Masukan username: ");
                            username = input.next();
                            System.out.print("Masukan password: ");
                            password = input.next();
                            System.out.println("-----");
                            System.out.print("Nama: ");
                            nama = input.next();
                            System.out.print("Update username: ");
                            String usernameBaru = input.next();
                            System.out.print("Update Password: ");
                            String passwordBaru = input.next();

                            JSONObject dataUserLogin = new JSONObject();
                            dataUserLogin.put("mode", "3");
                            dataUserLogin.put("username", username);
                            dataUserLogin.put("nilai1", nilai1);
                            dataUserLogin.put("passwordBaru", passwordBaru);

                            dout = new DataOutputStream(s.getOutputStream());
                            dout.writeUTF(String.valueOf(dataUserLogin));
                            dout.flush();

                        } else if (pilih.equals("b")) {

                        }*/

//                        JSONObject dataUserLogin = new JSONObject();
//                        dataUserLogin.put("mode", "3");
//                        dataUserLogin.put("username", username);
//                        dataUserLogin.put("password", password);
//
//                        dout = new DataOutputStream(s.getOutputStream());
//                        dout.writeUTF(String.valueOf(dataUserLogin));
//                        dout.flush();

                        System.out.print("Masukan username: ");
                        username = input.next();
                        System.out.print("Masukan password: ");
                        password = input.next();
                        System.out.print("Masukan nilai 1: ");
                        int nilai1 = input.nextInt();
                        System.out.print("Masukan nilai 2: ");
                        int nilai2 = input.nextInt();
                        System.out.print("Masukan nilai 3: ");
                        int nilai3 = input.nextInt();

                        ArrayList<Long> nilai = new ArrayList<>();
                        nilai.add((long) nilai1);
                        nilai.add((long) nilai2);
                        nilai.add((long) nilai3);


                        JSONObject dataUserLogin = new JSONObject();
                        dataUserLogin.put("mode", "3");
                        dataUserLogin.put("username", username);
                        dataUserLogin.put("password", password);
                        dataUserLogin.put("nilai", nilai);

                        dout = new DataOutputStream(s.getOutputStream());
                        dout.writeUTF(String.valueOf(dataUserLogin));
                        dout.flush();

                        break;
                    case 4:
                        JSONObject tampil = new JSONObject();
                        tampil.put("mode", "4");

                        dout = new DataOutputStream(s.getOutputStream());
                        dout.writeUTF(String.valueOf(tampil));
                        dout.flush();

                        System.out.println("Menunggu balasan server...");

                        din = new DataInputStream(s.getInputStream());
                        System.out.println(din.readUTF());

                        break;
                }
            }
            dout.close();
            din.close();
            s.close();

        }catch (Exception e) {
            System.out.println(e);
        }

    }

}
