package Week3Day4.Database.Assignment2;
import java.util.ArrayList;


public class Siswa {
    String username;
    String password;
    long id;
    String name;
    ArrayList<Long> niali;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Long> getNiali() {
        return niali;
    }

    public void setNiali(ArrayList<Long> niali) {
        this.niali = niali;
    }
}


