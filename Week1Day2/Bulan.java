package Week1Day2;
import java.util.Scanner;

public class Bulan {
    public static void main(String args[])
    {
        Scanner input = new Scanner(System.in);

        System.out.print("Nama = ");
        String nama = input.next();
        System.out.print("Tanggal Lahir = ");
        int tgl = input.nextInt();
        System.out.print("Bulan Lahir = ");
        int bln = input.nextInt();
        System.out.print("Tahun Lahir = ");
        int thn = input.nextInt();

        String sBln = "";
        if(bln == 1){
            sBln = "Januari";
        }else if(bln == 2){
            sBln  = "Februari";
        }else if(bln == 3){
            sBln  = "Maret";
        }else if(bln == 4){
            sBln  = "April";
        }else if(bln == 5){
            sBln  = "Mei";
        }else if(bln == 6){
            sBln  = "Juni";
        }else if(bln == 7){
            sBln  = "Juli";
        }else if(bln == 8){
            sBln  = "Agustus";
        }else if(bln == 9){
            sBln  = "September";
        }else if(bln == 10){
            sBln  = "Oktober";
        }else if(bln == 11){
            sBln  = "November";
        }else if(bln == 12){
            sBln  = "Desember";
        }

        int umur = 2022 - thn;

        System.out.println("Nama saya " + nama + ", lahir " + bln + " " + sBln + " " + thn + " berumur " + umur + " tahun.");

        // closing the scanner object
        input.close();
    }
}
