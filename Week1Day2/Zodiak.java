package Week1Day2;
import java.util.Scanner;

public class Zodiak {
    public static void main(String args[])
    {
        Scanner input = new Scanner(System.in);

        System.out.print("Nama = ");
        String nama = input.next();
        System.out.print("Tanggal Lahir = ");
        int tgl = input.nextInt();
        System.out.print("Bulan Lahir = ");
        int bln = input.nextInt();
        System.out.print("Tahun Lahir = ");
        int thn = input.nextInt();
        
        String sign;
        if (bln == 12 && tgl <= 23 || bln == 1 && tgl <= 20) {
            sign = "Capricorn";
        } else if (bln == 1 && tgl >= 21 || bln == 2 && tgl <= 18) {
            sign = "Aquarius";
        } else if (bln == 2 && tgl >= 19 || bln == 3 && tgl <=20) {
            sign = "Pisces";
        } else if (bln == 3 && tgl >= 21 || bln == 4 && tgl <=20) {
            sign = "Aries";
        } else if (bln == 4 && tgl >= 21 || bln == 5 && tgl <= 21) {
            sign = "Taurus";
        } else if (bln == 5 && tgl >= 22 || bln == 6 && tgl <= 21) {
            sign = "Gemini";
        } else if (bln == 6 && tgl >= 22 || bln == 7 &&  tgl <= 23) {
            sign = "Cancer";
        } else if (bln == 7 && tgl >= 24 || bln == 8 && tgl <= 23) {
            sign = "Leo";
        } else if (bln == 8 && tgl >= 24 || bln == 9 && tgl <=23) {
            sign = "Virgo";
        } else if (bln == 9 && tgl >= 24 || bln == 10 && tgl <=23) {
            sign = "Libra";
        } else if (bln == 10 && tgl >= 24 || bln == 11 && tgl <=22) {
            sign = "Scorpio";
        } else if (bln == 10 && tgl >= 23 || bln == 11 && tgl <=22) {
            sign = "Sagittarius";
        } else {
            sign = "Capricorn";
        }

        System.out.println("Nama saya "+nama+", zodiak nya adalah " + sign);

        // closing the scanner object
        input.close();
    }
}
