// use switch for simple/one decision, and use if for much decision
package Week1Day2;
import java.util.Scanner;
class Assigment2 {
 public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
        System.out.println("Masukan jenis oprasi apa (*,/,+,-)");
        char cOperator = input.next().charAt(0);
        System.out.println("Masukan bilangan pertama :");
        int iBilangan1 = input.nextInt();
        System.out.println("Masukan bilangan kedua :");
        int iBilangan2 = input.nextInt();
        
        int iOperasi;
        
        // switch statement to check size
        switch (cOperator) {
            case '*':
            iOperasi = iBilangan1 * iBilangan2;
            break;
            case '/':
            iOperasi = iBilangan1 / iBilangan2;
            break;
            case '+':
            iOperasi = iBilangan1 + iBilangan2;
            break;
            case '-':
            iOperasi = iBilangan1 - iBilangan2;
            break;
            default:
            iOperasi = 0;
            break;
        }
        System.out.println("Operasi antara "+iBilangan1+cOperator+iBilangan2+" = "+iOperasi);
    }
}