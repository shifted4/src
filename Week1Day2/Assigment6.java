package Week1Day2;
import java.util.Scanner;

public class Assigment6 {
    public static void main(String args[])
    {
        Scanner input = new Scanner(System.in);
        int pil=0;

        do {
            System.out.println("MENU");
            System.out.println("1. Volume Balok");
            System.out.println("2. Volume Bola");
            System.out.println("3. Hitung umur");
            System.out.println("4. EXIT");

            System.out.print("Input nomor : ");
            pil = input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    System.out.println("Menghitung Volume Balok = ");
                    System.out.print("Panjang: ");
                    int iPanjang = input.nextInt(); //input Int
                    System.out.print("Lebar: ");
                    int iLebar = input.nextInt(); //input Int
                    System.out.print("Tinggi: ");
                    int iTinggi = input.nextInt(); //input Int
                    int iVolumeBalok = iPanjang * iLebar * iTinggi; //logic volume
                    System.out.println("Volumenya = " +iVolumeBalok+ "kubik");
                    break;

                case 2:
                    System.out.println("Menghitung Volume Bola = ");
                    System.out.print("Phi: ");
                    float fPhi = input.nextFloat(); //float input
                    System.out.print("Jari - jari: ");
                    double dJarijari = input.nextDouble(); //double input
                    double dVolumeBola = 4 * fPhi * dJarijari * dJarijari * dJarijari /3; //logic volume ball
                    System.out.println("Volumenya = " +dVolumeBola+ "kubik");
                    break;

                case 3:
                    System.out.print("Tahun Lahir = ");
                    int thnLahir = input.nextInt();
                    System.out.print("Tahun Sekarang = ");
                    int thnSekarang = input.nextInt();

                    int n = thnSekarang - thnLahir;
                    // for loop  
                    for (int i = thnSekarang; i >= thnSekarang; i--) {
                    System.out.println(n);
                    }
                    break;

            }
            System.out.println();

        } while(pil != 4);

        // closing the scanner object
        input.close();
    }
}
