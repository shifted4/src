package Week3Day2.Assignment2;

import java.util.ArrayList;
import java.util.Scanner;

abstract class Worker{
    int IdKaryawan;
    String nama;
    int tunjanganPulsa, gajiPokok, absensi;


    public int getIdKaryawan() {
        return IdKaryawan;
    }

    public void setIdKaryawan(int idKaryawan) {
        IdKaryawan = idKaryawan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getTunjanganPulsa() {
        return tunjanganPulsa;
    }

    public void setTunjanganPulsa(int tunjanganPulsa) {
        this.tunjanganPulsa = tunjanganPulsa;
    }

    public int getGajiPokok() {
        return gajiPokok;
    }

    public void setGajiPokok(int gajiPokok) {
        this.gajiPokok = gajiPokok;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }
}


public class Staff extends Worker{
    int tunjanganMakan;
    ArrayList email;

    public int getTunjanganMakan() {
        return tunjanganMakan;
    }

    public void setTunjanganMakan(int tunjanganMakan) {
        this.tunjanganMakan = tunjanganMakan;
    }

    public ArrayList getEmail() {
        return email;
    }

    public void setEmail(ArrayList email) {
        this.email = email;
    }

    public static void main(String [] args){

        int id = 0;
        int gapok = 0;
        int absensi = 0;
        int tunjPulsa = 0;
        int tunjMakan = 0;
        String nama = "";
        String emailInput = "";
        ArrayList arrEmail = null;

        Staff st = new Staff();
        Scanner input = new Scanner(System.in);
        ArrayList arrStaff = new ArrayList();



        int pil = 0;
        do{
            System.out.println("");
            System.out.println("Menu : ");
            System.out.println("1. Buat Worker");
            System.out.println("2. Buat JSON Encode");
            System.out.println("3. Buat JDON Decode");
            System.out.println("4. Exit");

            System.out.print("Masukan pilihan : ");
            pil = input.nextInt();
            switch (pil){
                case 1:
                    int pilih2 = 0;
                    System.out.println("1. Tambah data Staff");
                    System.out.println("2. Tambah Data Manager");
                    System.out.print("Pilih : ");
                    pilih2 = input.nextInt();

                    switch (pilih2){
                        case 1:
                            System.out.println("Input Data Staff :");
                            System.out.println("ID : ");
                            id = input.nextInt();
                            st.setIdKaryawan(id);

                            System.out.println("Nama : ");
                            nama = input.next();
                            st.setNama(nama);

                            System.out.println("Gaji Pokok : ");
                            gapok = input.nextInt();
                            st.setGajiPokok(gapok);

                            System.out.println("Absensi : ");
                            absensi = input.nextInt();
                            st.setAbsensi(absensi);

                            System.out.println("Tunjangan Pulsa : ");
                            tunjPulsa = input.nextInt();
                            st.setTunjanganPulsa(tunjPulsa);

                            System.out.println("Tunjangan Makan : ");
                            tunjMakan = input.nextInt();
                            st.setTunjanganMakan(tunjMakan);

                            System.out.print("email = ");
                            emailInput = input.next();
                            arrEmail.add(emailInput);
                            st.setEmail(arrEmail);


                            arrStaff.add(st);
                            System.out.println("Data berhasil diinput");
                            break;
                        case 2 :
                            System.out.println("Input Data Manager");
                            break;

                    }
                    break;
                case 2 :
                    System.out.println("Encode File !");
                    break;
                case 3 :
                    System.out.println("Decode File");
                    break;
                case 4 :
                    System.out.println("Terima Kasih !");
                    break;

            }


        }while (pil != 4);
    }
}
