package Week3Day2.JSON;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
public class Tugas1 {

        public static void main(String[] args){
        /*
        {
            "name":"John",
            "age":30,
            "cars": ["Ford","BMW","Fiat"]
        }
        */

        /*List<String> ls = new ArrayList<>();

        ls.add("Ford");
        ls.add("BMW");
        ls.add("Fiat");*/

            JSONArray models1 = new JSONArray();
            models1.add("Fiesta");
            models1.add("Focus");
            models1.add("Mustang");

            JSONArray models2 = new JSONArray();
            models2.add("320");
            models2.add("X3");
            models2.add("X5");

            JSONArray models3 = new JSONArray();
            models3.add("500");
            models3.add("Panda");

            JSONObject car = new JSONObject();
            car.put("name", "Ford");
            car.put("models", models1);

            JSONObject car1 = new JSONObject();
            car1.put("name", "BMW");
            car1.put("models", models2);

            JSONObject car2 = new JSONObject();
            car2.put("name", "Fiat");
            car2.put("models", models3);

            JSONArray cars = new JSONArray();
            cars.add(car);
            cars.add(car1);
            cars.add(car2);

            JSONObject myObj = new JSONObject();
            myObj.put("name","John");
            myObj.put("age", 30);
            myObj.put("cars", cars);

            System.out.print(myObj);
        }
    }


