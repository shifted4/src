package Week3Day2.JSON;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;

public class Tugas2 {
        public static void main(String[] args) throws IOException, ParseException {

        /*
        {
            "name":"John",
            "age":30,
            "cars": ["Ford","BMW","Fiat"]
        }
        */

            JSONParser parser = new JSONParser();
            Reader reader = new StringReader("{\n" +
                    "  \"cars\": [\n" +
                    "    {\n" +
                    "      \"models\": [\n" +
                    "        \"Fiesta\",\n" +
                    "        \"Focus\",\n" +
                    "        \"Mustang\"\n" +
                    "      ],\n" +
                    "      \"name\": \"Ford\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"models\": [\n" +
                    "        \"320\",\n" +
                    "        \"X3\",\n" +
                    "        \"X5\"\n" +
                    "      ],\n" +
                    "      \"name\": \"BMW\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"models\": [\n" +
                    "        \"500\",\n" +
                    "        \"Panda\"\n" +
                    "      ],\n" +
                    "      \"name\": \"Fiat\"\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"name\": \"John\",\n" +
                    "  \"age\": 30\n" +
                    "}");

            Object jsonObj = parser.parse(reader);

            JSONObject jsonObject = (JSONObject) jsonObj;

            String name = (String) jsonObject.get("name");
            System.out.println("Name = " + name);

            long age = (Long) jsonObject.get("age");
            System.out.println("Age = " + age);

            JSONArray cars = (JSONArray) jsonObject.get("cars");

            for (int i =0; i < cars.size(); i++){
                JSONObject car = (JSONObject) cars.get(i);

                String nama = (String) car.get("name");
                System.out.println("Name :"+nama);

                JSONArray arrJSONModels = (JSONArray) car.get("models");
                ArrayList<String> arrModels;
                arrModels = (ArrayList<String>)  arrJSONModels;

                for (String lp:arrModels){
                    System.out.println("Models : "+lp);
                }

            }
        }

    }


