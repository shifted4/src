package Week3Day2.Assignment1;

import Week2Day3.Assignment2.Model.Staff;

import java.io.*;
import java.net.*;
import java.sql.SQLOutput;
import java.util.Scanner;

public class programSocketServer {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();

        ServerSocket ss = null;
        Socket s;
        DataInputStream din = null;
        DataOutputStream dout = null;
        String data = "";

        try {
            //read file
            FileReader fr=new FileReader("C:\\tmp\\file.txt");
            BufferedReader br=new BufferedReader(fr);

            int i;
            while((i=br.read())!=-1){
                data += (char) i;
            }

            System.out.println("read data file.txt - success");
            br.close();
            fr.close();

            // server up
            int port = Integer.parseInt(config.getPropValues("PORT"));
            ss = new ServerSocket(port);

            //stand by request
            System.out.println("Server menunggu request dari Client");
            s = ss.accept();

            //get request
            din = new DataInputStream(s.getInputStream());

            //sent request
            OutputStream outputStream = s.getOutputStream();
            dout = new DataOutputStream(outputStream);

            //dout from client
            String textDariClient = "";

            while (!textDariClient.equalsIgnoreCase("exit")) {
                //sent request
                dout = new DataOutputStream(s.getOutputStream());
                dout.writeUTF(data);
                dout.flush();

                //get request from client
                din = new DataInputStream(s.getInputStream());
                textDariClient = din.readUTF();
            }
            input.close();
            din.close();
            dout.close();
            ss.close(); //close server

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
