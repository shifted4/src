package Week2Day1.View.Assignment1;


import Week2Day1.Model.Mobil;

import java.util.Scanner;

public class Assignment1 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String userInputMerek;
        String userInputWarna;
        System.out.print("Masukan Merek Mobil: ");
        userInputMerek = input.nextLine();
        System.out.print("Masukan Warna Mobil: ");
        userInputWarna = input.nextLine();

        Mobil mobil1 = new Mobil();
        mobil1.sMerek = userInputMerek;
        mobil1.sWarna = userInputWarna;
        mobil1.info();
        mobil1.berjalan();
        mobil1.berhenti();
        System.out.print("Masukan Merek Mobil: ");
        userInputMerek = input.nextLine();
        System.out.print("Masukan Warna Mobil: ");
        userInputWarna = input.nextLine();

        Mobil mobil2 = new Mobil();
        mobil2.sMerek = userInputMerek;
        mobil2.sWarna = userInputWarna;
        mobil2.info();
        mobil2.berjalan();
        mobil2.berhenti();
        input.close();
    }
}
