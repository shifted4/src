package Week2Day1.View.Assignment2;
import java.util.*;

import static Week2Day1.Controller.Controller.*;


public class Assignment2 {

        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);
            int pilihan,num1,num2;
            do {
                System.out.println("Pilihan Menu : ");
                System.out.println("1. Penjumlahan");
                System.out.println("2. Pengurangan");
                System.out.println("3. Perkalian");
                System.out.println("4. Pembagian ");
                System.out.println("5. Exit");

                System.out.print("Pilihan menu =  ");
                pilihan = input.nextInt();

                System.out.print("Masukan bilangan pertama : ");
                num1 = input.nextInt();
                System.out.print("Masukan bilangan kedua : ");
                num2 = input.nextInt();
                switch (pilihan){
                    case 1:
                        int result = Penjumlahan(num1,num2);
                        System.out.println("Hasilnya adalah = " +result);
                        System.out.println("============================");
                        break;
                    case 2 :
                        int result1 = Pengurangan(num1,num2);
                        System.out.println("Hasilnya adalah = " +result1);
                        System.out.println("============================");
                        break;
                    case 3 :
                        int result2 = Perkalian(num1,num2);
                        System.out.println("Hasilnya adalah = " +result2);
                        System.out.println("============================");
                        break;
                    case 4 :
                        int result3 = Pembagian(num1,num2);
                        System.out.println("Hasilnya adalah = " +result3);
                        System.out.println("============================");
                        break;
                }
            }while(pilihan != 5);

        }
    }

