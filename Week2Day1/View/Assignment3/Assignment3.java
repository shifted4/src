package Week2Day1.View.Assignment3;

import java.util.Scanner;

import static Week2Day1.Controller.Controller.volBangun;

public class Assignment3 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan;

        do {
            System.out.println("Pilihan Menu : ");
            System.out.println("1. Volume Balok");
            System.out.println("2. Volume Bola");
            System.out.println("3. Volume Tabung");
            System.out.println("4. Exit ");

            System.out.print("Pilihan menu =  ");
            pilihan = input.nextInt();

            switch (pilihan){
                case 1:
                    int num1, num2, num3;
                    System.out.print("Masukan panjang: ");
                    num1 = input.nextInt();
                    System.out.print("Masukan lebar: ");
                    num2 = input.nextInt();
                    System.out.print("Masukan tinggi : ");
                    num3 = input.nextInt();
                    int result = volBangun(num1,num2,num3);
                    System.out.println("Volume Balok = " +result);
                    System.out.println("============================");
                    break;
                case 2 :
                    double numb1;
                    int numb2;
                    System.out.print("Masukan phi : ");
                    numb1 = input.nextDouble();
                    System.out.print("Masukan jari-jari : ");
                    numb2 = input.nextInt();
                    double result1 = volBangun(numb1,numb2);
                    System.out.println("Volume Bola = " +result1);
                    System.out.println("============================");
                    break;
                case 3 :
                    double number1;
                    int number2,number3;
                    System.out.print("Masukan phi : ");
                    number1 = input.nextDouble();
                    System.out.print("Masukan jari-jari : ");
                    number2 = input.nextInt();
                    System.out.print("Masukan tinggi : ");
                    number3 = input.nextInt();
                    double result2 = volBangun(number1,number2,number3);
                    System.out.println("Volume Tabung = " +result2);
                    System.out.println("============================");
                    break;
            }
        }while(pilihan != 4);

    }
}

