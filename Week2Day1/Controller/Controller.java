package Week2Day1.Controller;

public class Controller {
    // Method Defined Assignment2 - Kalkulator Sederhana with method & 2 parameters
    public static int Penjumlahan(int x, int y){
        return x + y;
    }
    public static int Pengurangan(int x, int y){
        return x - y;
    }
    public static int Perkalian(int x, int y){
        return x * y;
    }
    public static int Pembagian(int x, int y){
        return x / y;
    }

    // Method Defined Assignment3 - Volume Bangun with method overload & parameters
    public static int volBangun (int panjang, int lebar, int tinggi){
        return panjang * lebar * tinggi;
    }
    public static double volBangun(double jari, double phi){
        return 4 * phi * jari * jari * jari /3;
    }
    public static double volBangun(double phi, int jari, int tinggi){
        return phi * jari * jari * tinggi;
    }
}
