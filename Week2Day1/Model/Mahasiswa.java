package Week2Day1.Model;

public class Mahasiswa {
    String nama, jurusan;
    int nilaiFisika, nilaiKimia, nilaiBiologi;
    public Mahasiswa(){
        this.nama = "aldi";
        this.nilaiBiologi = 7;
        this.nilaiFisika = 5;
        this.nilaiKimia = 7;
        String parse = nama+","+nilaiBiologi+","+nilaiFisika+","+nilaiKimia;
        String [] parsedS = parse.split(",");
        for (String el:parsedS){
            System.out.print(el);
        }
    }

    public Mahasiswa(String nama, String jurusan){
        this.nama = nama;
        this.jurusan = jurusan;
        this.nilaiBiologi = 0;
        this.nilaiFisika = 0;
        this.nilaiKimia = 0;
        System.out.println(this.nama+ " "+this.jurusan);
        System.out.println(this.nilaiFisika+" "+this.nilaiKimia+" "+this.nilaiBiologi);
    }

    public Mahasiswa(String nama1, String jurusan1, int fisika, int kimia, int biologi){
        this.nama = nama1;
        this.jurusan = jurusan1;
        this.nilaiBiologi = biologi;
        this.nilaiFisika = fisika;
        this.nilaiKimia = kimia;
        System.out.println(this.nama+ " "+this.jurusan);
        System.out.println(this.nilaiFisika+" "+this.nilaiKimia+" "+this.nilaiBiologi);
    }

    public Mahasiswa(String nama, int fisika, int kimia, int biologi){
        this.nama = nama;
        this.nilaiBiologi = biologi;
        this.nilaiFisika = fisika;
        this.nilaiKimia = kimia;
        System.out.println(this.nama+ "," +nilaiFisika+","+nilaiKimia+","+nilaiBiologi);
    }
}

