package Week2Day1.Model;

import java.util.*;


public class Mobil {
    public String sMerek;
    public String sWarna;
    boolean bStatus;

    public void info(){
        Scanner input = new Scanner(System.in);

        System.out.println("Merek Mobil : ");
        sMerek = input.next();
        System.out.println("Warna Mobil : ");
        sWarna = input.next();
    }

    public void berjalan(){
        bStatus = true;
        System.out.println(sMerek+" warna "+sWarna+" sedang berjalan");
    }

    public void berhenti(){
        bStatus = false;
        System.out.println(sMerek+" warna "+sWarna+" sedang berhenti");
    }




}
