package Week2Day5.Assignment2.View;

import Week2Day5.Assignment2.Controller.MahasiswaController;
import Week2Day5.Assignment2.Model.Mahasiswa;

import java.util.Scanner;

public class MahasiswaApp {
    public static void main(String args[]) {

        MahasiswaController MC = new MahasiswaController();
        Scanner input = new Scanner(System.in);
        int pil=0;

        //Login Proccess
        MC.proccessLogin();

        do {
            System.out.println("---------------------------");
            System.out.println("==========> MENU <=========");
            System.out.println("1. Buat Data Mahasiswa");
            System.out.println("2. Edit & Hapus Data Mahasiswa");
            System.out.println("3. Laporan Mahasiswa");
            System.out.println("4. EXIT");
            System.out.println("---------------------------");

            System.out.print("Input nomor : ");
            pil = input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    MC.addMahasiswa();
                    break;

                case 2:
                    System.out.println("a. Edit");
                    System.out.println("b. Delete");
                    System.out.print("Input huruf : ");
                    String sPil = input.next();

                    switch (sPil){
                        case "a":
                            MC.viewMahasiswa();
                            MC.editMahasiswa();
                            break;
                        case "b":
                            MC.viewMahasiswa();
                            MC.deleteMahasiswa();
                            break;
                        default:
                            System.out.println("pilih a atau b saja !");
                            break;
                    }
                    break;
                case 3:
                    MC.viewMahasiswa();
                    break;

                default:
                    System.out.println("Isi sesuai pilihan 1 - 4 !");
                    break;

            }
            System.out.println();

        } while(pil != 4);

    }
}
