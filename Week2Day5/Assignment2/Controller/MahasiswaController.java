package Week2Day5.Assignment2.Controller;


import Week2Day5.Assignment2.Model.Mahasiswa;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Pattern;

public class MahasiswaController {

    Map m = new HashMap();
    Scanner input = new Scanner(System.in);

    boolean auth;
    boolean checkRegex;
    String strUsername = "";
    String strPassword = "";



    public void proccessLogin(){

        //Read File Username
        try {

            FileReader fr=new FileReader("C:\\tmp\\Username.txt");
            BufferedReader br=new BufferedReader(fr);

            int i;
            while((i=br.read())!=-1){
                strUsername = strUsername + (char)i;
            }

            br.close();
            fr.close();

        }catch (Exception e){
            System.out.println(e);
        }

        //Read File Password
        try {

            FileReader fr=new FileReader("C:\\tmp\\Password.txt");
            BufferedReader br=new BufferedReader(fr);

            int i;
            while((i=br.read())!=-1){
                strPassword = strPassword + (char)i;
            }

            br.close();
            fr.close();

        }catch (Exception e){
            System.out.println(e);
        }

        while (auth = true) {

            System.out.println("======> Login <======");
            System.out.print("Username : ");
            String username = input.next();
            System.out.print("Password : ");
            String password = input.next();


            String regexU = "^[A-Za-z0-9+_.-]+@(.+)$";
            boolean regexUsername = Pattern.matches(regexU, username);
            String regexP = ".{8,32}" + "[a-zA-z]*" + "\\w*";
            boolean regexPassword = Pattern.matches(regexP, password);

            if (regexUsername == true && regexPassword == true){
                checkRegex = true;
            }else {
                checkRegex = false;
            }

            if (username.equals(strUsername) == true && password.equals(strPassword) == true && checkRegex == true) {
                    System.out.println(">>>>>>>>>>>>>> Login Sukse <<<<<<<<<<<<<<");
                    System.out.println("Selamat \"" + username + "\" Anda Berhasil LOGIN !");
                    System.out.println(">>>>>>>>>>>>>>>>>Selamat!<<<<<<<<<<<<<<<<");
                    auth = true;
                    break;
            } else {
                    System.out.println("Maaf Username dan Password SALAH !");
                    auth = false;
            }

        }
    }
    public void addMahasiswa(){

        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.input.next();

        Mahasiswa mhs ;
        ArrayList arrNilai = new ArrayList();
        for (int i = 1; i <=3; i++){
            if (i == 1){
                System.out.print("Masukan nilai bahas inggris : ");
            }else if (i == 2){
                System.out.print("Masukan nilai fisika : ");
            }else if (i == 3) {
                System.out.print("Masukan nilai algoritma :");
            }
            int nilaiInput = input.nextInt();
            arrNilai.add(nilaiInput);
        }
        m.put(id, new Mahasiswa(id, nama, arrNilai));
    }
    public void editMahasiswa(){

        System.out.print("Masukin ID: ");
        int id = this.input.nextInt();

        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Mahasiswa
            Mahasiswa mhs = (Mahasiswa) entry.getValue();

            if (mhs.getId() == id) {
                System.out.print("Masukan perubahan : ");
                String nama = input.next();
                m.replace(id, nama);
                System.out.println("Berhasil merubah data id "+id+ " dengan perubahan "+nama);
            }
        }
    }

    public void deleteMahasiswa(){

        System.out.print("Masukin ID: ");
        int id = this.input.nextInt();

        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Mahasiswa
            Mahasiswa mhs = (Mahasiswa) entry.getValue();

            if (mhs.getId() == id) {
                m.remove(id);
                System.out.println("Berhasil menghapus data ID = "+id);
            }
        }
    }
    public void viewMahasiswa(){
        Set set=m.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        System.out.println("ID\tNama\tBhs Inggris\tFisika\tAlgoritma");

        while(itr.hasNext()){
            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //soorting by key
            m.entrySet().stream().sorted(Map.Entry.comparingByKey());
            //Convert to Mhs
            Mahasiswa mhs = (Mahasiswa) entry.getValue();
            System.out.println(entry.getKey()+"\t"+mhs.getNama()+"\t\t   "+mhs.getArrNilai().get(0)+"\t\t   "+mhs.getArrNilai().get(1)+"\t    "+mhs.getArrNilai().get(2));
        }

    }

}
