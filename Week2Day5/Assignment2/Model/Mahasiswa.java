package Week2Day5.Assignment2.Model;

import java.util.ArrayList;

public class Mahasiswa {
    int id;
    String nama;
    ArrayList<Double> arrNilai;

    public Mahasiswa(int id, String nama, ArrayList arrNilai) {
        this.id = id;
        this.nama = nama;
        this.arrNilai = arrNilai;
    }

    public ArrayList getArrNilai() {
        return arrNilai;
    }

    public void setArrNilai(ArrayList arrNilai) {
        this.arrNilai = arrNilai;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
