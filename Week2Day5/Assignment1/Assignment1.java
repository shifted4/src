package Week2Day5.Assignment1;

import java.util.*;
import java.util.regex.Pattern;

public class Assignment1 {
    public static void main(String args[]){
        Scanner input = new Scanner(System.in);
        System.out.println("======> Login <======");
        System.out.print("Username : ");
        String username = input.next();
        System.out.print("Password : ");
        String password = input.next();
        System.out.println("=====================");

        String regexUsername = "^[A-Za-z0-9+_.-]+@(.+)$";
        System.out.println("Hasil Validasi Username Kamu = "+Pattern.matches(regexUsername, username)+"\n -------------------\n Selamat datang "+username+"\n -------------------");

        String regexPassword = ".{8,99}"+"[a-zA-z]*"+"\\w*";
        System.out.println("Hasil Validasi Password Kamu = "+Pattern.matches(regexPassword, password)+"\n -------------------\n Password kamu Tepat \""+password+"\""+"\n -------------------");

        if (Pattern.matches(regexUsername, username) == true && Pattern.matches(regexPassword, password) == true){
            System.out.println("============");
            System.out.println("Anda LOGIN !");
            System.out.println("============");
        }else {
            System.out.println("Login dulu");
        }
    }
}
