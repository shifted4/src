package Week2Day5.Assignment4.Controller;

import Week2Day5.Assignment4.Model.Mhs;

import java.util.*;

public class MhsController extends Thread{


    public Scanner input = new Scanner(System.in);

    Map m = new HashMap();


    public void run(){
            try {
                System.out.println("");
                Thread.sleep(250);
                Set set=m.entrySet();//Converting to Set so that we can traverse
                Iterator itr=set.iterator();
                System.out.println("ID\tNama\tBhs Inggris\tFisika\tAlgoritma");

                while(itr.hasNext()){
                    //Converting to Map.Entry so that we can get key and value separately
                    Map.Entry entry=(Map.Entry)itr.next();
                    //soorting by key
                    m.entrySet().stream().sorted(Map.Entry.comparingByKey());
                    //Convert to Mhs
                    Mhs mhs = (Mhs) entry.getValue();
                    ArrayList nilai = mhs.getNilai();
                    System.out.println(entry.getKey()+"\t"+mhs.getNama()+"\t\t   "+nilai.get(0)+"\t\t   "+nilai.get(1)+"\t    "+nilai.get(2));
                }
                System.out.println("Success...");
            } catch (InterruptedException e) {
                System.out.println(e);
            }

    }

    public void createDummy(){
        System.out.println("Creating Dummy Data");
    }

    //Case 1
    //method menambahkan staff
    public void addMahasiswa(){

        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.input.next();

        ArrayList  nilai = new ArrayList();
        for (int i = 1; i <=3; i++){
            if (i == 1){
                System.out.print("Masukan nilai bahas inggris : ");
            }else if (i == 2){
                System.out.print("Masukan nilai fisika : ");
            }else if (i == 3) {
                System.out.print("Masukan nilai algoritma :");
            }
            int nilaiInput = input.nextInt();
            nilai.add(nilaiInput);
        }
        m.put(id, new Mhs (id, nama, nilai));
    }
    public void viewMahasiswa(){
        Set set=m.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        System.out.println("ID\tNama\tBhs Inggris\tFisika\tAlgoritma");

        while(itr.hasNext()){
            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //soorting by key
            m.entrySet().stream().sorted(Map.Entry.comparingByKey());
            //Convert to Mhs
            Mhs mhs = (Mhs) entry.getValue();
            ArrayList nilai = mhs.getNilai();
            System.out.println(entry.getKey()+"\t"+mhs.getNama()+"\t\t   "+nilai.get(0)+"\t\t   "+nilai.get(1)+"\t    "+nilai.get(2));
        }

    }



}
