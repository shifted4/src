package Week2Day5.Assignment4.Model;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.*;

import Week2Day5.Assignment4.Controller.MhsController;
public class Mhs extends Thread{
    int id;
    String nama;
    double  inggris, fisika, algoritma;
    ArrayList nilai ;

    public Mhs(int id, String nama, ArrayList nilai) {
        this.id = id;
        this.nama = nama;
        this.nilai = nilai;
    }

    public ArrayList getNilai() {
        return nilai;
    }

    public void setNilai(ArrayList nilai) {
        this.nilai = nilai;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getInggris() {
        return inggris;
    }

    public void setInggris(double inggris) {
        this.inggris = inggris;
    }

    public double getFisika() {
        return fisika;
    }

    public void setFisika(double fisika) {
        this.fisika = fisika;
    }

    public double getAlgoritma() {
        return algoritma;
    }

    public void setAlgoritma(double algoritma) {
        this.algoritma = algoritma;
    }

    public void run(){
        try {

            Map m = new HashMap();
            Set set=m.entrySet();//Converting to Set so that we can traverse
            Iterator itr=set.iterator();
            while(itr.hasNext()){
                //Converting to Map.Entry so that we can get key and value separately
                Map.Entry entry=(Map.Entry)itr.next();
                //soorting by key
                m.entrySet().stream().sorted(Map.Entry.comparingByKey());
                //Convert to Mhs
                Mhs mhs = (Mhs) entry.getValue();
                ArrayList nilai = mhs.getNilai();
                //instansisasi file writer
                FileWriter fw = new FileWriter("C:\\tmp\\DataMahasiswa.txt"); //lokasi direktori
                fw.write("ID : \t  Nama : \n Bhs Inggris : \t Fisika : \t Algoritma"); //create header

                fw.write(mhs.getId()+ " \t " + mhs.getNama() + " \t " + nilai.get(0)+ " \t " + nilai.get(1)+ " \t " + nilai.get(2));

                fw.close();
            }

            //skenario dijalankan jika error
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Success membuat file !");
    }

}
