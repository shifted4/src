package Week1Day3;
import java.util.Scanner; //import for input

class Assigment3{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int iPilihan = 0;
        int iInputArray;
        int [] iArray = new int[10];

    do {
        System.out.println("Menu :");
        System.out.println("1. Input Data Array integer");
        System.out.println("2. Sort Array");
        System.out.println("3. Cari Bilangan");
        System.out.println("4. Exit");
        System.out.print("Pilih Menu = ");

        iPilihan = input.nextInt();
        
        switch (iPilihan){
            //input panjang aray 
            case 1 :
                System.out.print("Masukan panjang array =");
                iInputArray = input.nextInt();
                iArray = new int[iInputArray];
                //perulangan input indeks array
                for (int i = 0; i < iInputArray; i++){
                    System.out.print("Index ["+i+"]= ");
                    iArray[i]=input.nextInt();
                }
                System.out.print("Array yang diinput [ ");
                //perulangan output indeks array
                for (int i : iArray)
                    System.out.print(i + "");
                System.out.println("] \n");
                break;

            case 2 :
                //mengurutkan data menggunakan buble sort
                for (int i = 0; i < iArray.length - 1; i++) {
                    for (int j = 0; j < iArray.length - 1 - i; j++) { 
                        if (iArray[j + 1] < iArray[j]) {
                            int temp = iArray[j];
                            iArray[j] = iArray[j + 1];
                            iArray[j + 1] = temp;
                        }
                    }
                }
                System.out.println("Berhasil melakukan sorting!");
                System.out.print("[ ");
                //perulangan untuk menampilkan hasil array yang urut
                for (int i : iArray)
                    System.out.print(i + "");
                System.out.println("] \n");
                break;

            case 3 :
                //pencarian menggunakan binary search
                System.out.println("Input angka yang akan dicari : ");
                int target =  input.nextInt();
                int left = 0;
                int middle;
                int right = iArray.length - 1;
                boolean bCari = false;
                //pengkondisian jika data yg diinput tidak ada
                if (target < iArray[0] || target > iArray[iArray.length -1]){
                    System.out.println("angka tidak  ditemukan ! \n");
                    break;
                }
                    //binary search algorithm
                    while (left <= right) {
                        middle = (left + right) / 2;
                        if (iArray[middle] == target) {
                            System.out.println("Angka ketemu di index ke  " + middle);
                            bCari = true;
                            System.out.println("\n");
                            break;
                        } else if (iArray[middle] < target) {
                            left = middle + 1;
                        } else if (iArray[middle] > target) {
                            right = middle - 1;
                        }
                    }
                    //pengkondisian apabila data tidak ditemukan
                    if (!bCari) {
                        System.out.println("Angka yang di cari tidak ada di dalam array ! \n");
                    }
                break;
        }      
    }
    while(iPilihan != 4);
    input.close();
    }
}