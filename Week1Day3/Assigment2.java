package Week1Day3;

import java.util.Scanner; //import for input
class Assigment2{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("Baris array integer = ");
        int iBaris = input.nextInt(); 
        System.out.print("Kolom array integer = ");
        int iKolom = input.nextInt(); 

        //membuat multi dimensi array
        int [][]  Array = new int[iBaris][iKolom];

        //input panjang aray dan input indeks array
        System.out.println("Masukan elemen array =");
        for (int i = 0; i < iBaris && i < iKolom; ++i){
            for (int j=0; j < Array[i].length; ++j) {
                System.out.print("Array["+i+"]["+j+"] : ");
                Array[i][j]=input.nextInt(); 
            }
        }

        //menampilkan array multidimensi
        System.out.println("Baris dan Kolom Array 2 dimensi adalah = ["+iBaris+","+iKolom+"]");
        System.out.println("Array multidimensi 2 Dimensi");
        for (int i=0;  i < iBaris && i < iKolom; i++){
            for (int j=0; j < Array[i].length; ++j) {  
            System.out.print("  "+Array[i][j]+"\t");
            }
        System.out.println(" ");
        }
        input.close();
    }
}
