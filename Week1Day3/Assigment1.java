package Week1Day3;

import java.util.Scanner; //import for input
class Assigment1{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in); // initialitation for input
        System.out.print("Masukan Panjang Integer =");
        int iPanjangArray = input.nextInt(); //input length array

        int [] array = new int[10]; //create array & alocate 10 
        System.out.println("Masukan elemen array =");
        for(int i=0; i<iPanjangArray; i++){ //loop length array
            System.out.print("Index ["+i+"]= ");
            array[i]=input.nextInt(); //input 
        }
        //menampilkan isi array dan jumlah baris
        System.out.println("Dengan jumlah baris adalah = "+iPanjangArray);
        System.out.println("Elemen Arraynya adalah = ");
        for (int i=0; i<iPanjangArray; i++){
            System.out.print("  "+array[i]);
        }
        input.close();
    }
}