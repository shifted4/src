//package yang diimport untuk mengambil input dari user
package Week1Day3;
import java.util.*;

class Assignment4{
    public static void main(String[] args){
        //membuat objek mobil
        class Mobil{
            String merek;
            String warna;
            int tahunPembuatan;
            //menginisiasi objek mobil dengan property merek, warna dan tahun pembuatan
            Mobil(String merek, String warna, int tahunPembuatan){
                this.merek = merek;
                this.warna = warna;
                this.tahunPembuatan = tahunPembuatan;
            }
        }
        //membuat array list
        ArrayList<Mobil> mbl = new ArrayList<Mobil>();

        Scanner inputString = new Scanner(System.in);
        Scanner inputInteger = new Scanner(System.in);
        int iPilihan = 0;
        
    do {
        System.out.println("Menu :");
        System.out.println("1. Input objek");
        System.out.println("2. Tampilkan objek");
        System.out.println("3. Exit");
        System.out.print("Pilih menu = ");
        iPilihan = inputInteger.nextInt();

        switch (iPilihan){
            //membuat instansi baru berdasarkan input user
            case 1 :
                String merek,warna;
                int tahunPembuatan;
                System.out.println("Masukan merek = ");
                merek = inputString.next();
                System.out.println("Masukan warna = ");
                warna = inputString.next();
                System.out.println("Masukan tahun pembuatan = ");
                tahunPembuatan = inputInteger.nextInt();
                
                //Creating user-defined class objects 
                Mobil mobil1 = new Mobil(merek, warna,tahunPembuatan);
                mbl.add(mobil1);
                System.out.println(" ");
                break; 

            //mencetak objek yang sudah diinputkan
            case 2 :
                Iterator itr = mbl.iterator();
                while (itr.hasNext()){
                    Mobil mb = (Mobil) itr.next();
                    System.out.println("");
                    System.out.println("Merek : " + mb.merek);
                    System.out.println("Warnaa : " + mb.warna);
                    System.out.println("Tahun Pembuatan : " + mb.tahunPembuatan);
                    System.out.println("");
                }
                break;
            
            case 3 :
                System.out.println("Keluar Program 3....2....1...");
                break;

            default :
                System.out.println("Pilih menu 1 sampai 3 saja yang tersedia !");
                break;
        }      
    } while(iPilihan != 3);
    inputInteger.close();
    inputString.close();
 }
}
    
