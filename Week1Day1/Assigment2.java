package Week1Day1;
class Assigment2 {
    
 public static void main(String[] args) {
    
        //declare variables with value
        boolean bBoolean = true;
        byte bByte = 120;
        short sShort = 240;
        int iInt = 2000;
        long lLong = 10000;
        double dDouble = 64.64;
        float fFloat = 16.16f;
        char cChar = 'z'; 
        String sString = "G2Academy";

        //output with call variables
        System.out.println("boolean");
        System.out.println(bBoolean);
        System.out.println("byte");
        System.out.println(bByte);
        System.out.println("short");
        System.out.println(sShort);
        System.out.println("int");
        System.out.println(iInt);
        System.out.println("long");
        System.out.println(lLong);
        System.out.println("double");
        System.out.println(dDouble);
        System.out.println("float");
        System.out.println(fFloat);
        System.out.println("char");
        System.out.println(cChar);
        System.out.println("string");
        System.out.println(sString);
    }
}