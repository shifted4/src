package Week1Day1;

class Assigment1{
    //method special
    public static void main(String[] args){
        //create 4 variables
        String nama = "Aldi aprilianto";
        String gender = "Laki-laki";
        int umur = 22;
        double nilaiRata = 8.5;
        //output with 4 variables
        System.out.println("Seorang Siswa "
            +gender+" bernama "
            +nama+", berumur "
            +umur+" tahun, memiliki nilai rata-rata "
            +nilaiRata);
    }
}