//import library scanner for input
package Week1Day1;
import java.util.Scanner;
class Assegment4 {
    public static void main(String[] args) {

        //create object input
        Scanner input = new Scanner(System.in);
 
        System.out.println("Menghitung Volume Balok = ");
        System.out.print("Panjang: ");
        int iPanjang = input.nextInt(); //input Int
        System.out.print("Lebar: ");
        int iLebar = input.nextInt(); //input Int
        System.out.print("Tinggi: ");
        int iTinggi = input.nextInt(); //input Int
        int iVolumeBalok = iPanjang * iLebar * iTinggi; //logic volume
        System.out.println("Volumenya = " +iVolumeBalok+ "kubik");
        
        System.out.println("============================");

        System.out.println("Menghitung Volume Bola = ");
        System.out.print("Phi: ");
        float fPhi = input.nextFloat(); //float input
        System.out.print("Jari - jari: ");
        double dJarijari = input.nextDouble(); //double input
        double dVolumeBola = 4 * fPhi * dJarijari * dJarijari * dJarijari /3; //logic volume ball
        System.out.println("Volumenya = " +dVolumeBola+ "kubik");
    }
}
