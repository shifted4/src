package Week1Day1;

class Assigment3 {
    
 public static void main(String[] args) {

    //declare variables for Luas Persegi
    int iPanjang = 12;
    int iLebar = 8;

    //logic Luas Persegi
    int iLuasPersegi= iPanjang * iLebar;

    //output Luas Persegi
    System.out.println(iLuasPersegi);

    //declare variables for Luas Segitiga
    float fAlas = 7.0f;
    float fTinggi = 3.5f;

    //logic Luas Segitiga
    float fLuasSegitiga =fAlas * fTinggi/2;

    //output Luas Segitiga
    System.out.println(fLuasSegitiga);

    //condition expression
    if (iLuasPersegi > fLuasSegitiga){
        System.out.println(true);
    }else{
        System.out.println(false);
    }

    //declare variables
    int result1, result2;

    // original value
    System.out.println("Value of a: " + iLuasPersegi);

    // increment operator
    result1 = ++iLuasPersegi;
    System.out.println("After increment: " + result1);
    System.out.println("Value of b: " + iLuasPersegi);

    // decrement operator
    result2 = --iLuasPersegi;
    System.out.println("After decrement: " + result2);

    }
}