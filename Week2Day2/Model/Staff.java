package Week2Day2.Model;

public class Staff {
    public int id, gaji_pokok, total_gajji;
    public int absensi =20;
    public int result_tunjangan_makan = absensi*20000;
    public int result_tunjangan_transport = absensi*50000;

    public int total_gaji = result_tunjangan_makan + result_tunjangan_transport + get_gaji_pokok();
    public String nama, pengumpulan;

    public int get_id() {
        return id;
    }
    public void set_id(int id){
        this.id = id;
    }

    public String get_nama() {
        return nama;
    }

    public void set_nama(String nama) {
        this.nama = nama;
    }

    public int get_gaji_pokok() {
        return gaji_pokok;
    }

    public void set_gaji_pokok(int gaji_pokok) {
        this.gaji_pokok = gaji_pokok;
    }

    public String get_pengumpulan() {
        return pengumpulan;
    }

    public void set_pengumpulan(String pengumpulan) {
        this.pengumpulan = pengumpulan;
    }

    public int get_absensi() {
        return absensi;
    }

    public void set_absensi(int absensi) {
        this.absensi = absensi;
    }

    public void absensi_staff(){

    }
    public void tunjangan_makan(){
        result_tunjangan_makan =  get_absensi() * 20000;
    }
    public static int tunjangan_transport(int get_absensi){
        return get_absensi * 50000;
    }
}
