package Week2Day2.View.Exam1;

import Week2Day2.Model.Staff;

import java.util.*;

public class Main {

    public  static void main(String args[]){
    Scanner input_int = new Scanner(System.in);
    Scanner input_string = new Scanner(System.in);
    ArrayList<Staff> array_staff = new ArrayList<>();
    int pilihan =0;

        while(pilihan < 99) {
            System.out.println("MENU :");
            System.out.println("1. Buat Staff ");
            System.out.println("2. Edit atau Delete Staff");
            System.out.println("3. Tampilkan Data Semua Staff ");
            System.out.println("4. Absensi Staff ");
            System.out.println("5. Hitung Tunjangan");
            System.out.println("6. Hitung Total Gaji");
            System.out.println("7. Laporan Gaji (Sort by ID)");
            System.out.println("99. Exit");

            System.out.print("Masukan Pilihan : ");
            pilihan = input_int.nextInt();
            int absensi =20;
            int total_gaji;

            switch (pilihan) {
                case 1:
                    Staff obj_staff = new Staff();

                    int id = 0;
                    int gaji_pokok= 0 ;
                    String nama = "";


                    System.out.print("Masukan ID Staff : ");
                    id = input_int.nextInt();
                    obj_staff.set_id(id);

                    System.out.print("Masukan Nama Staff dengan ID \""+obj_staff.id+"\" : ");
                    nama = input_string.next();
                    obj_staff.set_nama(nama);

                    System.out.print("Masukan Gaji dari staff \""+obj_staff.nama+ "\": ");
                    gaji_pokok = input_int.nextInt();
                    obj_staff.set_gaji_pokok(gaji_pokok);

                    obj_staff.set_absensi(absensi);

                    //jadikan satu string
                    obj_staff.set_pengumpulan(id+","+nama+","+gaji_pokok+","+absensi);

                    //menambahkan data obj_staff ke array
                    array_staff.add(obj_staff);

                    //allert
                    System.out.println("------------------------------------------------");
                    System.out.println("Pemberitahuan !!! \n\"Data berhasil ditambahkan di array list\"");
                    System.out.println("------------------------------------------------");
                    break;
                case 2:
                    System.out.println("Data Staff");
                    System.out.println("-------------------------------");
                    for (Staff m:array_staff) {
                        String accu = m.get_pengumpulan();
                        String [] parsedAccu = accu.split(",");
                        System.out.println(parsedAccu[0] + "-" + parsedAccu[1] + "-" +parsedAccu[2]+ "-" +parsedAccu[3]);
                    }
                    System.out.println("-------------------------------");
                    int pilihan2=0;
                    while (pilihan2 < 3){
                        System.out.println("Menu Edit dan Delete");
                        System.out.println("1. Edit Staff");
                        System.out.println("2. Delete Staff dari Array List");
                        System.out.print("Pilihan : ");
                        pilihan2 = input_int.nextInt();
                        System.out.println("-------------------------------");
                        switch (pilihan2){
                            case 1 :
                                System.out.print("masukan id :");
                                int edit_id = input_int.nextInt();
                                System.out.print("masukan perubahan");
                                String edit_field = input_string.next();
                                //untuk update masih belum bisa dikarenakan edit_field harus merubah array list Staff menjadi String
                                //array_staff.set(edit_id, edit_field);
                                System.out.println("===Data Berhasil Dirubah===");
                                break;
                            case 2 :
                                System.out.print("masukan id yang akann di hapus:");
                                int delete_id = input_int.nextInt();
                                array_staff.remove(delete_id);
                                System.out.println("===Data dengan id \""+delete_id+"\" Berhasil Dihapus===");
                                break;
                            case 3 :
                                System.out.println("Keluar Ke menu utama !");
                                break;
                            default:
                                System.out.println("input 1-2 untuk pilihan, 3 untuk exit");
                                break;
                        }
                    }

                    System.out.print("");
                    break;
                case 3:
                    System.out.println("===> Data Staff <===");
                    for (Staff m:array_staff) {
                        String accu = m.get_pengumpulan();
                        String [] parsedAccu = accu.split(",");
                        System.out.println("\n ID : "+parsedAccu[0]+"\n Nama : " +parsedAccu[1] + "\n Gaji Pokok : " +parsedAccu[2] + "\n Absensi : " +parsedAccu[3]);
                    }
                    break;

                case 4:

                    System.out.println("Menambahkan absensi");
                    System.out.print("Masukan id :");
                    int edit_absensi_id = input_int.nextInt();
                    System.out.println("Id yang diinput"+edit_absensi_id);
                    //niat nya adalah ketika kita sudah input idnya
                    //selanjutnya diarahkan ke method untuk dilakukan increment +1
                    //kalau mau menambahkan lagi di id yang sama masukan kembali seperti awal
                    //untuk update masih belum bisa dikarenakan edit_absensi_id harus merubah array lit Staff menjadi Integer
                    break;

                case 5:
                    //masih menggunakan manual count:) belom bisa pake method karena didalam array list ada objek

                    Staff obj_staff1 = new Staff();
                    System.out.print("Total Seluruh Tunjangan Makanan Staff = ");
                    System.out.println(obj_staff1.result_tunjangan_makan * array_staff.size());
                    System.out.print("Total Seluruh Tunjangan Transport Staff = ");
                    System.out.println(obj_staff1.result_tunjangan_transport * array_staff.size());
                    System.out.println();
//                    System.out.println("Hitung Tunjangan Makan dan Transport");
//                    int tunjangan_makan = absensi * 20000;
//                    int tunjangan_transport = absensi * 50000;
//                    int total_tunjangan = tunjangan_makan + tunjangan_transport;
//                    System.out.println(total_tunjangan);
//                    Iterator itr = array_staff.iterator();
//                    while (itr.hasNext()) {
//                        Staff a = (Staff) itr.next();
//
//                        System.out.println("Data Staff= "+a.nama+"  "+a.result_tunjangan_makan+"  "+a.result_tunjangan_transport);
//                    }

                    break;

                case 6:
                    Staff obj_staff2 = new Staff();
                    System.out.println("Total Gaji Per Staff = "+obj_staff2.total_gaji);
                    break;

                case 7:
                    System.out.println("Data Total Gajian & Sort By ID");
                    System.out.println("------------------------------------------");
                    System.out.println("ID \tNama \tAbsensi \tGaji \tTerbilang");
                    //pengurutan datanya menggunakan bubble sort
                    //troubel karena didalam array list ada objek
//                    for (int i = 0; i < array_staff.size() - 1; i++) {
//                        for (int j = 0; j < array_staff.size() - 1 - i; j++) {
//                            if (array_staff.size(j + 1) < array_staff.size(j)) {
//                                int temp = array_staff[j];
//                                array_staff[j] = array_staff[j + 1];
//                                array_staff[j + 1] = temp;
//                            }
//                        }
//                    }
                    Iterator itr = array_staff.iterator();
                    while (itr.hasNext()) {
                        Staff a = (Staff) itr.next();
                        System.out.println(a.get_id()+"\t"+a.get_nama()+"\t"+a.get_absensi()+"\t\t\t"+a.get_gaji_pokok()+"\t Dua Juta Rupiah");
                    }
                    break;

                case 99:
                    System.out.println("===============================");
                    System.out.println("Terima Kasih... keluar dalam...");
                    System.out.print("3......");
                    System.out.print("2......");
                    System.out.print("1......");
                    System.out.println("========> Have A Nice Day <==========");
                    break;
                default:
                    System.out.println("Input antara 1 - 7 untuk pilihan, 99 untuk keluar!");
                    break;
            }
        }
        input_int.close();
        input_string.close();
    }
}
