package Week3Day3.Exam2;

import Week3Day2.Assignment1.CrunchifyGetPropertyValues;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.*;
import java.util.*;

public class ServerSiswa {

    Scanner input = new Scanner(System.in);
    Week3Day2.Assignment1.CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();

    ServerSocket ss = null;
    Socket s;
    DataInputStream din = null;
    DataOutputStream dout = null;

    String textDariClient = "";
    String textKeClient = "";

    String usernameL = "";
    String passwordL = "";

    String usernameR = "";
    String passwordR = "";


    //server up
    public void serverUp(){
        try {
            // server up
            int port = Integer.parseInt(config.getPropValues("PORT"));
            ss = new ServerSocket(port);

            //stand by request
            System.out.println("Server menunggu request dari Client");
            s = ss.accept();

            //get request
            InputStream inputStream = s.getInputStream();
            din = new DataInputStream(inputStream);
            OutputStream outputStream = s.getOutputStream();
            dout = new DataOutputStream(outputStream);

            JSONRegister();
            JSONLogin();
            editData();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void JSONRegister () throws Exception{
        textDariClient = (String) din.readUTF();
        System.out.println("Pesan dari Client: " + textDariClient);

        //decode JSON
        JSONParser parser = new JSONParser();
        Reader reader = new StringReader(textDariClient);
        Object jsonObj = parser.parse(reader);
        JSONObject register = (JSONObject) jsonObj;

        long id = (long) register.get("id");
        usernameR = (String) register.get("username");
        passwordR = (String) register.get("password");
        String name = (String) register.get("name");

        FileWriter fw = new FileWriter("C:\\tmp\\User.txt"); //lokasi direktori
        //convert string to byte
        fw.write("id,username,password,name\n ");
        fw.write(id+","+usernameR+","+passwordR+","+name+"\n");
        fw.close();
        System.out.println("Write File User.txt Success...");
    }

    public void JSONLogin () throws Exception{
        textDariClient = (String) din.readUTF();
        System.out.println("Pesan dari Client: " + textDariClient);

        JSONParser parser = new JSONParser();
        Reader reader = new StringReader(textDariClient);
        Object jsonObj = parser.parse(reader);
        JSONObject login = (JSONObject) jsonObj;
        usernameL = (String) login.get("username");
        passwordL = (String) login.get("password");

        if (usernameL.equals(usernameR) && passwordL.equals(passwordR)) {
            String auth = "true";
            dout.writeUTF(auth);
            dout.flush();
            System.out.println(">>>>>>>>>>>>>> Login Sukse <<<<<<<<<<<<<<");
        } else {
            String auth = "false";
            dout.writeUTF(auth);
            dout.flush();
            System.out.println("Maaf Username dan Password SALAH !");
        }
    }
    public void serverDown() throws Exception{
        try {
            textDariClient = (String) din.readUTF();
            System.out.println("Pesan dari Client: " + textDariClient);
            if (textKeClient.equals("exit")){
                input.close();
                din.close();
                dout.close();
                ss.close();
            }else{
                System.out.println("Masih Lanjut");
            }

        }catch (Exception e){
            System.out.println(e);
        }
    }

    public void editData(){
        //define data yang ada di file user.txt dengan decode JSON, tapi masih error di login
        //edit data by key yang ada pada JSON
        //data ditemukan lalu put dengan new value
        //tampilkan kembali seluruh isi file User.txt
    }
    public static void main(String args[]) throws Exception {

        ServerSiswa serv = new ServerSiswa();
        serv.serverUp();
        serv.serverDown();
    }

}