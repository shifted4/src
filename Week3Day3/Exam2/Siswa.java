package Week3Day3.Exam2;

import java.util.ArrayList;

public class Siswa {
    long id;
    String username;
    String password;
    String name;
//    ArrayList<Long> nilai;

    public Siswa(long a, String b, String c, String d) {
        this.id = a;
        this.username = b;
        this.password = c;
        this.name = d;
//        this.nilai = e;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String toString() {
        return ("\nID : " + this.id +
                "\nUsername : " + this.username +
                "\nPassword : " + this.password +
                "\nName : " + this.name);
    }

}