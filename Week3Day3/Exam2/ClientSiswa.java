package Week3Day3.Exam2;


import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.*;

class kirimFTP extends Thread{
    public void run() {
        String server = "ftp.dharmakertamandiri.co.id";
        int port = 21;
        String user = "adikbtpns@demo.dharmakertamandiri.co.id";
        String pass = "Shifted12345";

        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // APPROACH #1: uploads first file using an InputStream
            File firstLocalFile = new File("C:/tmp/Aldi_user.txt");

            String firstRemoteFile = "Aldi_user.txt";
            InputStream inputStream = new FileInputStream(firstLocalFile);

            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            inputStream.close();
            if (done) {
                System.out.println("The first file is uploaded successfully.");
            }

        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
public class ClientSiswa {
    public static void main(String[] args) throws IOException {
        Socket s = null;
        DataInputStream din = null;
        DataOutputStream dout = null;
        Scanner input = new Scanner(System.in);
        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
        int port = 0;
        String ip ="";
        String dataFromServer ="";
        String formatDataFromServer = "";
        String [] parseKalimat = null;
        JSONObject register = new JSONObject();
        JSONObject login = new JSONObject();
        boolean auth ;
        String textDariServer = "";
        Siswa sw ;

        int pil =0;
        do {
            System.out.println("---------------------------");
            System.out.println("==========> MENU <=========");
            System.out.println("1. Register");
            System.out.println("2. Login");
            System.out.println("3. Update Data");
            System.out.println("4. Multi Thread");
            System.out.println("5. Exit");
            System.out.println("---------------------------");

            System.out.print("Input nomor : ");
            pil = input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    try {
                        //Register
                        System.out.println("//---> Register <--//");
                        System.out.print("Masukan Id : ");
                        long inputId = input.nextInt();
                        System.out.print("Masukan Username : ");
                        String inputUsername = input.next();
                        System.out.print("Masukan Password : ");
                        String inputPassword = input.next();
                        System.out.print("Masukan Name : ");
                        String inputName = input.next();

                        sw = new Siswa(inputId, inputUsername, inputPassword, inputName);
                        //insert to JSON
                        register.put("id",sw.getId() );
                        register.put("username",sw.getUsername());
                        register.put("password",sw.getPassword());
                        register.put("name",sw.getName());


                        //Mengakses config.properties
                        port = Integer.parseInt(config.getPropValues("PORT"));
                        ip = config.getPropValues("IP");

                        // Membuat sebuah stream socket dan menghubungkan ke port & ip yang telah di spesifikasikan
                        s = new Socket(ip,port);

                        String textKeServer = "";
                        //sent request
                        dout = new DataOutputStream(s.getOutputStream());
                        dout.writeUTF(register.toJSONString());
                        dout.flush();


                    } catch (Exception e) {
                        System.out.println(e);
                    }

                    break;

                case 2:

                    boolean checkRegex;

//                    String usernameFile = "";
//                    String passwordFile = "";
//                    try {
//                        FileReader fr = new FileReader("C:\\tmp\\User.txt");
//                        BufferedReader br = new BufferedReader(fr);
//
//                        int i;
//                        String str = "";
//                        while ((i = br.read()) != -1) {
//                            str = str + (char) i;
//                        }
//
//                        String[] parseBySentence = str.split("\n");
//                        for (int index = 1; index < parseBySentence.length; index++) {
//                            String[] parsedByWord = parseBySentence[index].split(",");
//                            usernameFile = parsedByWord[1];
//                            passwordFile = parsedByWord[2];
//                        }
//
//                        br.close();
//                        fr.close();
//                    }catch (Exception e){
//                        System.out.println(e);
//                    }

                    try {
                        // login
                        System.out.println("//---> LOGIN <---//");
                        System.out.print("Masukan Username : ");
                        String username = input.next();
                        System.out.print("Masukan Password : ");
                        String password = input.next();


                        String regexU = "^[A-Za-z0-9+_.-]+@(.+)$";
                        boolean regexUsername = Pattern.matches(regexU, username);
                        String regexP = ".{8,99}" + "[a-zA-z]*" + "\\w*";
                        boolean regexPassword = Pattern.matches(regexP, password);

                        if (regexUsername == true && regexPassword == true) {
                            checkRegex = true;
                        } else {
                            checkRegex = false;
                        }

                        //encode JSON
                        login.put("username", username);
                        login.put("password", password);

                        //Kirim file ke server
                        dout = new DataOutputStream(s.getOutputStream());
                        dout.writeUTF(login.toJSONString());
                        dout.flush();
                    }catch (Exception e){
                        System.out.println(e);
                    }

                        //terima text
                        InputStream inputStream = s.getInputStream();
                        din = new DataInputStream(inputStream);
                        textDariServer = (String) din.readUTF();
                        System.out.println("Pesan dari Server: " + textDariServer);
                        if (textDariServer.equals("true")){
                            System.out.println("Login SUkses");
                            break;
                        }else {
                            System.out.println("Register dulu !");
                        }
//                    try {
//                        parseKalimat = dataFromServer.split("\n");
//                        for (String satuKalimat: parseKalimat) {
//                            String [] satuKata = satuKalimat.split(",");
//                            formatDataFromServer += "Nama : "+satuKata[0]+ "\nNilai Fisika : "+satuKata[1]+"\nNilai Kimia : "+satuKata[2]+"\nNilai Biologi : "+satuKata[3]+"\n\n";
//
//                        }
//
//                        //instansisasi file writer
//                        FileWriter fw = new FileWriter("C:\\tmp\\FileProses_Aldi.txt"); //lokasi direktori
//                        //convert string to byte
//                        fw.write(formatDataFromServer);
//                        fw.close();
//
//
//                        System.out.println("Success...");
//
//                    } catch (Exception e) {
//                        System.out.println(e);
//                    }

                    break;
                case 3:
                    //define data yang ada di file user.txt dengan decode JSON, tapi masih error di login
                    //edit data by key yang ada pada JSON
                    //data ditemukan lalu put dengan new value
                    //tampilkan kembali seluruh isi file User.txt
                    break;

                case 4 :
                    //edit data sesuai no 3
                    //mengirimkan data ket file user.txt
                    //berbarengan dengan kirim file FTP ke server kak hary
                    kirimFTP kirim = new kirimFTP();
                    kirim.start();
                    break;
                case 5:
                    try {
                        dout = new DataOutputStream(s.getOutputStream());
                        dout.writeUTF("exit");
                        dout.flush();

                        System.out.println("Close all connections");

                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;

                default:
                    System.out.println("Isi sesuai pilihan 1 - 4 !");
                    break;

            }
            System.out.println();

        } while(pil != 99);


    }

}