package Week2Day3.Assignment2.View;

import Week2Day3.Assignment2.Controller.StaffController;
import Week2Day3.Assignment2.Model.Staff;

import java.util.ArrayList;
import java.util.Scanner;

public class StaffApp {
    public static void main(String args[]) {

        //instansiasi objek controller
        StaffController sc = new StaffController();

        //instansiasi objek input dengan scanner
        Scanner input = new Scanner(System.in);
        int pil=0;

        //looping menu
        do {
            System.out.println("------------------------>");
            System.out.println("=======> MENU <=======");
            System.out.println("0. Buat Dummy Data");
            System.out.println("1. Buat Staff");
            System.out.println("2. Edit or Delete Staff");
            System.out.println("3. Tampilkan Data Staff");
            System.out.println("4. Absensi Staff");
            System.out.println("5. Hitung Tunjangan");
            System.out.println("6. Hitung Total Gaji");
            System.out.println("7. Laporan Gaji");
            System.out.println("8. Simpan Data Staff");
            System.out.println("9. Load Data Staff");
            System.out.println("------------------------>");
            System.out.println("99. EXIT");

            System.out.print("Input nomor : ");
            pil = input.nextInt();

            switch (pil) {
                // pilihan kondisi menu
                case 1:
                    //memanggil method input staff
                    sc.addStaff();
                    break;

                case 2:
                    //input & memanggil method edit dan delete
                    System.out.println("a. Edit");
                    System.out.println("b. Delete");
                    System.out.print("Input huruf : ");
                    String sPil = input.next();

                    switch (sPil){
                        case "a":
                            sc.editStaff();
                            break;
                        case "b":
                            sc.deleteStaff();
                            break;
                    }
                    break;

                case 3:
                    //memanggil method menampilkan staff
                    sc.viewStaff();
                    break;
                case 4:
                    //memanggil method absensi
                    sc.absenStaff();
                    break;
                case 5:
                    //memanggil method menghitung tunjangan
                    sc.hitungTunjangan();
                    break;
                case 6 :
                    //memanggil method menghitung gaji
                    sc.hitungTotalGaji();
                    break;
                case 7:
                    //memanggil method sorting
                    sc.viewStaffSort();
                    break;
                case 8:
                    //memanggil method menyimpan data ke file
                    sc.simpanData();
                    break;
                case 9 :
                    //memanggil method menampilkan data dari file
                    sc.loadData();
                    break;

                case 0:
                    //memanggil method menambahkan banyak data
                    sc.createDummy();
                    break;
            }
            System.out.println();

        } while(pil != 99);

        // closing the scanner object
        input.close();

    }
}
