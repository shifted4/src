package Week2Day3.Assignment2.Controller;

import Week2Day3.Assignment2.Model.Staff;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
public class StaffController {

    //instansiasi array list
    ArrayList<Staff> arrStaff = new ArrayList<>();

    //instansiasi input scanner
    Scanner input = new Scanner(System.in);

    //membuat variabel global
    int resultTmakan, resultTtrasport, totalTunjangan, totalGaji;

    //Case 0
    //method data dummy
    public void createDummy(){
        System.out.println("Creating Dummy Data");
        this.arrStaff.add(new Staff(2,"PETER",3000));
        this.arrStaff.add(new Staff(1,"JOHN",2000));
    }

    //Case 1
    //method menambahkan staff
    public void addStaff(){

        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.input.next();
        System.out.print("Input Gaji Pokok : ");
        int gapok = this.input.nextInt();

        //instansiasi objek model kelas staff
        Staff st = new Staff(id, nama, gapok);
        //menambahkan objek ke array list
        this.arrStaff.add(st);
    }

    //Case 2.a
    //method edit staff
    public void editStaff(){

        int i = 0;

        System.out.print("Masukan ID yang akan di Edit : ");
        int id = this.input.nextInt();

        //looping array list
        for (Staff st:this.arrStaff) {
            //pencarian indeks berdasarkan id
            if(id == st.getId()){
                System.out.println("ID Found at index "+i); // Dapet index nya, index ke i
                //set ulang data dari id yg diinput
                System.out.print("Ganti nama menjadi : ");
                this.arrStaff.get(i).setNama(this.input.next());
                break;
            }
            //increment +1
            i++;
        }

    }
    //Case 2.b
    //method delete staff
    public void deleteStaff(){

        int i = 0;
        System.out.print("Masukan ID yang akan di Delete : ");
        int id = this.input.nextInt();
        //looping array list
        for (Staff st:this.arrStaff) {
            //mencari indeks berdasarkan id
            if(id == st.getId()){
                System.out.println("ID Found at index "+i); // Dapet index nya, index ke i
                //hapus indeks arraylist dengan id yg diinput
                this.arrStaff.remove(i);
                break;
            }
            //increment
            i++;
        }
    }

    //Case 3
    //method menampilkan data staff
    public void viewStaff(){
        //looping index array list
        for (Staff st:this.arrStaff) {
            System.out.println("ID : "+st.getId());
            System.out.println("Nama : "+st.getNama());
            System.out.println("Gaji Pokok : "+st.getGapok());
            System.out.println("Absensi : "+st.getAbsensi());
        }
    }

    //Case 4
    //Method Absensi staff
    public void absenStaff(){

        int i = 0;

        System.out.print("Masukan ID Absensi : ");
        int id = this.input.nextInt();
        //looping array list
        for (Staff st:this.arrStaff) {
            //mencari indeks berdasarkan id
            if(id == st.getId()){
                System.out.println("ID Found at index "+i); // Dapet index nya, index ke i
                //memasukan data absensi = 20 ke variabel
                int absenCounter = this.arrStaff.get(i).getAbsensi();
                System.out.println("Absensi "+st.getNama()+" = "+absenCounter);
                //absensi ditambah 1
                absenCounter = absenCounter + 1;
                //update arraylist
                this.arrStaff.get(i).setAbsensi(absenCounter);
                System.out.println("Absensi "+st.getNama()+" bertambah menjadi = "+absenCounter);
                break;
            }
            //increment
            i++;
        }
    }

    //Case 5
    //Method menghitung tunjangan
    public void hitungTunjangan(){

        System.out.print("Menghitung Seluruh Tunjangan Staff : ");
        //looping array list
        for (Staff st:this.arrStaff) {
            System.out.println("Nama : "+st.getNama());
            System.out.println("Absensi : "+st.getAbsensi());
        }
        //looping array list dan logic tunjangan
        for (Staff st:this.arrStaff){
            resultTmakan = st.getAbsensi() * 20000;
            resultTtrasport = st.getAbsensi() * 50000;
            totalTunjangan = resultTmakan + resultTtrasport;
            //menampilkan hasil
            System.out.println("===================================");
            System.out.println("Nama : "+st.getNama());
            System.out.println("Gaji Pokok : "+st.getGapok());
            System.out.println("Absensi : "+st.getAbsensi());
            System.out.println("Total Tunjangan : "+totalTunjangan);
            System.out.println("===================================");
        }

    }

    //Case 6
    //Method total gaji
    public void hitungTotalGaji(){
        System.out.print("Menghitung Seluruh Total Gaji Staff : ");
        //looping array list dan logic
        for (Staff st:this.arrStaff){
            totalGaji = st.getGapok() + totalTunjangan;

            System.out.println("===================================");
            System.out.println("Nama : "+st.getNama());
            System.out.println("Absensi : "+st.getAbsensi());
            System.out.println("Total Gaji : "+totalGaji);
            System.out.println("===================================");
        }
    }

    //Case 7
    //method menampilkan sorting
    public void viewStaffSort(){
        System.out.println("id\tNama\t\tAbsensi\tGaji Total\t\tTerbilang");

        //sorting menggunakan collections berdasarkan id
        Collections.sort(this.arrStaff, Comparator.comparingLong(Staff::getId));

        //looping array list yang sudah di sort
        for (Staff st:this.arrStaff) {
            String terbilang = angkaToTerbilang((long)totalGaji);
            System.out.println(st.getId()+"\t"+st.getNama()+"\t\t"+st.getAbsensi()+"\t"+totalGaji+"\t\t"+terbilang);
        }
    }

    //array terbilang
    static String[] huruf={"","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"};

    //method untuk menambilkan int menjadi terbilang
    public String angkaToTerbilang(Long angka){
        if(angka < 12)
            return huruf[angka.intValue()];
        if(angka >=12 && angka <= 19)
            return huruf[angka.intValue() % 10] + " Belas";
        if(angka >= 20 && angka <= 99)
            return angkaToTerbilang(angka / 10) + " Puluh " + huruf[angka.intValue() % 10];
        if(angka >= 100 && angka <= 199)
            return "Seratus " + angkaToTerbilang(angka % 100);
        if(angka >= 200 && angka <= 999)
            return angkaToTerbilang(angka / 100) + " Ratus " + angkaToTerbilang(angka % 100);
        if(angka >= 1000 && angka <= 1999)
            return "Seribu " + angkaToTerbilang(angka % 1000);
        if(angka >= 2000 && angka <= 999999)
            return angkaToTerbilang(angka / 1000) + " Ribu " + angkaToTerbilang(angka % 1000);
        if(angka >= 1000000 && angka <= 999999999)
            return angkaToTerbilang(angka / 1000000) + " Juta " + angkaToTerbilang(angka % 1000000);
        if(angka >= 1000000000 && angka <= 999999999999L)
            return angkaToTerbilang(angka / 1000000000) + " Milyar " + angkaToTerbilang(angka % 1000000000);
        if(angka >= 1000000000000L && angka <= 999999999999999L)
            return angkaToTerbilang(angka / 1000000000000L) + " Triliun " + angkaToTerbilang(angka % 1000000000000L);
        if(angka >= 1000000000000000L && angka <= 999999999999999999L)
            return angkaToTerbilang(angka / 1000000000000000L) + " Quadrilyun " + angkaToTerbilang(angka % 1000000000000000L);
        return "";
    }

    //Case 8
    //method membuat data file
    public void simpanData(){
        System.out.print("Masukan Nama Folder : ");
        String inputDir = input.next();
        System.out.print("Masukan Nama File : ");
        String inputFile = input.next();
            //skenario dijalankan jika sukses
            try {
                //instansisasi file writer
                FileWriter fw = new FileWriter("C:\\"+inputDir+"\\"+inputFile); //lokasi direktori
                fw.write("ID, Nama, Total Gaji \n"); //create header

                //looping array list
                for (Staff st :arrStaff){
                    //menulis isi file
                    fw.write(st.getId()+ "," + st.getNama() + "," +totalGaji+"\n");
                }
                //close agar bisa digunakan lagi
                fw.close();

            //skenario dijalankan jika error
            } catch (Exception e) {
                System.out.println(e);
            }
        System.out.println("Success...");
    }

    //Case 9
    //method menampilkan data dari file
    public void loadData(){
        System.out.print("Masukan Nama Folder : ");
        String bacaDir = input.next();
        System.out.print("Masukan Nama File : ");
        String bacaFile = input.next();
        //percobaan dijalankan jika sukses
        try {
            //instansisasi file reader
            FileReader fr=new FileReader("C:\\"+bacaDir+"\\"+bacaFile); //lokasi direktori
            //instansiasi buffer reader
            BufferedReader br=new BufferedReader(fr);

            //membuat variabel indek dan untuk merubah char menjadi string
            int i;
            String str = "";
            //loping menerjemahkan isi char pada file
            while((i=br.read())!=-1){
                //tipe data apapun apabila ditambahkan dengan string maka akan menjadi string
                str = str + (char)i;
            }

//            System.out.print(str);
            //membuat array yang berisi indeks yang dipisahkan berdasarkan \n
            String [] parsedS = str.split("\n");

            //looping array parsedS
            for (int index = 1; index < parsedS.length; index++ ){
//                System.out.println(parsedS[index]);
                //membuat array untuk memisahkan string berdasarkan ,
                String [] parsedS2 = parsedS[index].split(",");
//                System.out.println(parsedS2[0]+parsedS2[1]);

                //merubah data irisan yang string menjadi integer
                int id = Integer.parseInt(parsedS2[0]);
                int gaji = Integer.parseInt(parsedS2[2]);
                // 1,JOHN,1402000
                String nama = parsedS2[1];
                //instansiasi objek dengan 3 parameter
                Staff st = new Staff(id, nama, totalGaji, 0);
                //menambahkan objek ke array list
                arrStaff.add(st);
            }
            br.close(); //close buffer reader
            fr.close(); //close file reader

            //looping array list
            for (Staff data: arrStaff) {
                System.out.println("======================================");
                System.out.println(" ID : "+data.getId());
                System.out.println(" Nama : "+data.getNama());
                System.out.println(" Total Gaji : "+totalGaji);
                //System.out.println(" Gaji Pokok : "+data.getGapok());
                //membuat variabel terbilang dengan method yang sudah dibuat
                String terbilang = angkaToTerbilang((long)totalGaji);
                System.out.println(" Terbilang : "+terbilang);
                System.out.println("======================================");
                System.out.println("  ");
            }
        //percobaan dijalankan jika erorr
        }catch (Exception e){
           System.out.println(e);
        }
    }


}
