package Week2Day3.Assignment1;

import java.io.*;
import java.util.Scanner;

public class Assignment1 {
    public static void main(String args[])  {
        Scanner inputInt = new Scanner(System.in);
        Scanner inputString = new Scanner(System.in);

        int pil = 0;
        String inputDir, inputFile, inputIsiFile;
        String bacaDir, bacaFile;
        while (pil < 3){
            System.out.println("   ");
            System.out.println("----------------------");
            System.out.println("=======> Menu <=======");
            System.out.println("1. Tulis String");
            System.out.println("2. Baca String");
            System.out.println("----------------------");
            System.out.print("Masukan Pilihan = ");
            pil = inputInt.nextInt();
            switch (pil) {
                case 1:
                    //input data
                    System.out.print("Input Direktori = ");
                    inputDir = inputString.next();
                    System.out.print("Input Nama File = ");
                    inputFile = inputString.next();
                    System.out.print("Input Isi File = ");
                    inputIsiFile = inputString.next();

                    //logic data
                    try {
                        FileOutputStream fout = new FileOutputStream("C:\\" + inputDir + "\\" + inputFile);
                        BufferedOutputStream bout = new BufferedOutputStream(fout);
                        String s = inputIsiFile;
                        byte b[] = s.getBytes();
                        bout.write(b);
                        bout.flush();
                        bout.close();
                        fout.close();
                        System.out.println("success");
                    } catch (Exception e) {

                    }
                    break;
                case 2:
                    //input
                    System.out.print("Input Nama Direktori = ");
                    bacaDir = inputString.next();
                    System.out.print("Input Nama FIle = ");
                    bacaFile = inputString.next();

                    //logic
                    try {
                        FileInputStream fin = new FileInputStream("C:\\" + bacaDir + "\\" + bacaFile);
                        BufferedInputStream bin = new BufferedInputStream(fin);
                        int i;
                        while ((i = bin.read()) != -1) {
                            System.out.print((char) i);
                        }
                        bin.close();
                        fin.close();
                    } catch (Exception e) {
                        System.out.println("======>Isi data "+bacaFile+"<======");
                        System.out.println(e);
                        System.out.println("=================");
                    }

                    break;
            }
        }
        inputInt.close();
        inputString.close();
    }
}
